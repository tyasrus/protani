package com.irvandha.protani.model;

/**
 * kelas Komentar berfungsi sebagai model yang mewakili data dari tabel komentar
 */
public class Komentar {

    //variabel id untuk menampung data id
    private int id;
    //variabel tanaman untuk menampung data tanaman
    private Tanaman tanaman;
    //variabel jenis untuk menampung data jenis
    private Jenis jenis;
    //variabel nama untuk menampung data nama
    private String nama;
    //variabel gejala untuk menampung data gejala
    private String gejala;
    //variabel perawatan untuk menampung data perawatan
    private String perawatan;
    //variabel namaPengirim untuk menampung data namaPengirim
    private String namaPengirim;
    //variabel emailPengirim untuk menampung data emailPengirim
    private String emailPengirim;
    //variabel status untuk menampung data status
    private boolean status;

    //constructor untuk inisiasi kelas
    public Komentar() {
    }

    //constructor untuk inisiasi kelas dengan parameter id
    public Komentar(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel id
    public int getId() {
        return id;
    }

    //method untuk memberikan value ke variabel id
    public void setId(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel tanaman
    public Tanaman getTanaman() {
        return tanaman;
    }

    //method untuk memberikan value ke variabel tanaman
    public void setTanaman(Tanaman tanaman) {
        this.tanaman = tanaman;
    }

    //method untuk mengambil value dari variabel jenis
    public Jenis getJenis() {
        return jenis;
    }

    //method untuk memberikan value ke variabel jenis
    public void setJenis(Jenis jenis) {
        this.jenis = jenis;
    }

    //method untuk mengambil value dari variabel nama
    public String getNama() {
        return nama;
    }

    //method untuk memberikan value ke variabel nama
    public void setNama(String nama) {
        this.nama = nama;
    }

    //method untuk mengambil value dari variabel gejala
    public String getGejala() {
        return gejala;
    }

    //method untuk memberikan value ke variabel gejala
    public void setGejala(String gejala) {
        this.gejala = gejala;
    }

    //method untuk mengambil value dari variabel perawatan
    public String getPerawatan() {
        return perawatan;
    }

    //method untuk memberikan value ke variabel perawatan
    public void setPerawatan(String perawatan) {
        this.perawatan = perawatan;
    }

    //method untuk mengambil value dari variabel namaPengirim
    public String getNamaPengirim() {
        return namaPengirim;
    }

    //method untuk memberikan value ke variabel namaPengirim
    public void setNamaPengirim(String namaPengirim) {
        this.namaPengirim = namaPengirim;
    }

    //method untuk mengambil value dari variabel emailPengirim
    public String getEmailPengirim() {
        return emailPengirim;
    }

    //method untuk memberikan value ke variabel emailPengirim
    public void setEmailPengirim(String emailPengirim) {
        this.emailPengirim = emailPengirim;
    }

    //method untuk mengambil value dari variabel status
    public boolean isStatus() {
        return status;
    }

    //method untuk memberikan value ke variabel status
    public void setStatus(boolean status) {
        this.status = status;
    }

    //method toString untuk mencetak model ini kedalam bentuk string (plain text)
    @Override public String toString() {
        return "Komentar{"
            + "id="
            + id
            + ", tanaman="
            + tanaman
            + ", jenis="
            + jenis
            + ", nama='"
            + nama
            + '\''
            + ", gejala='"
            + gejala
            + '\''
            + ", perawatan='"
            + perawatan
            + '\''
            + ", namaPengirim='"
            + namaPengirim
            + '\''
            + ", emailPengirim='"
            + emailPengirim
            + '\''
            + ", status="
            + status
            + '}';
    }

    /**
     * kelas Jenis berfungsi sebagai model yang mewakili data dari tabel jenis_komentar
     */
    public static class Jenis {

        //variabel id untuk menampung data id
        private int id;
        //variabel nama untuk menampung data nama
        private String nama;

        //constructor untuk inisiasi kelas
        public Jenis() {
        }

        //constructor untuk inisiasi kelas dengan parameter id
        public Jenis(int id) {
            this.id = id;
        }

        //method untuk mengambil value dari variabel id
        public int getId() {
            return id;
        }

        //method untuk memberikan value ke variabel id
        public void setId(int id) {
            this.id = id;
        }

        //method untuk mengambil value dari variabel nama
        public String getNama() {
            return nama;
        }

        //method untuk memberikan value ke variabel nama
        public void setNama(String nama) {
            this.nama = nama;
        }

        //method toString untuk mencetak model ini kedalam bentuk string (plain text)
        @Override public String toString() {
            return "Jenis{" + "id=" + id + ", nama='" + nama + '\'' + '}';
        }
    }
}
