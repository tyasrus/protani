package com.irvandha.protani.model;

/**
 * kelas User berfungsi sebagai model yang mewakili data dari tabel user
 */
public class User {

    //variabel id untuk menampung data id
    private int id;
    //variabel nama untuk menampung data name
    private String name;
    //variabel password untuk menampung data password
    private String password;

    //constructor untuk inisiasi kelas
    public User() {
    }

    //constructor untuk inisiasi kelas dengan parameter id
    public User(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel id
    public int getId() {
        return id;
    }

    //method untuk memberikan value ke variabel id
    public void setId(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel name
    public String getName() {
        return name;
    }

    //method untuk memberikan value ke variabel name
    public void setName(String name) {
        this.name = name;
    }

    //method untuk mengambil value dari variabel password
    public String getPassword() {
        return password;
    }

    //method untuk memberikan value ke variabel password
    public void setPassword(String password) {
        this.password = password;
    }

    //method toString untuk mencetak model ini kedalam bentuk string (plain text)
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
