package com.irvandha.protani.model;

/**
 * kelas Tanaman berfungsi sebagai model yang mewakili data dari tabel tanaman
 */
public class Tanaman {

    //variabel id untuk menampung data id
    private int id;
    //variabel nama untuk menampung data nama
    private String nama;
    //variabel deskripsi untuk menampung data deskripsi
    private String deskripsi;
    //variabel status untuk menampung data status
    private boolean status;

    //constructor untuk inisiasi kelas
    public Tanaman() {
    }

    //constructor untuk inisiasi kelas dengan parameter id
    public Tanaman(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel id
    public int getId() {
        return id;
    }

    //method untuk memberikan value ke variabel id
    public void setId(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel nama
    public String getNama() {
        return nama;
    }

    //method untuk memberikan value ke variabel nama
    public void setNama(String nama) {
        this.nama = nama;
    }

    //method untuk mengambil value dari variabel deskripsi
    public String getDeskripsi() {
        return deskripsi;
    }

    //method untuk memberikan value ke variabel deskripsi
    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    //method untuk mengambil value dari variabel status
    public boolean isStatus() {
        return status;
    }

    //method untuk memberikan value ke variabel status
    public void setStatus(boolean status) {
        this.status = status;
    }

    //method toString untuk mencetak model ini kedalam bentuk string (plain text)
    @Override public String toString() {
        return "Tanaman{"
            + "id="
            + id
            + ", nama='"
            + nama
            + '\''
            + ", deskripsi='"
            + deskripsi
            + '\''
            + ", status="
            + status
            + '}';
    }
}
