package com.irvandha.protani.ui.komentar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.service.KomentarService;
import com.irvandha.protani.service.config.BaseView;

/**
 * kelas ini berfungsi sebagai tampilan detail komentar serta untuk menghapus komentar
 */
public class DetailActivity extends AppCompatActivity
    implements BaseView<Komentar>, KomentarService.SaveView, KomentarService.DeleteView,
    KomentarService.GetView {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Toolbar dengan id R.id.toolbar dengan nama variabel toolbar
  @BindView(R.id.toolbar) Toolbar toolbar;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_first dengan nama variabel tvFirst
  @BindView(R.id.tv_first) TextView tvFirst;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Button dengan id R.id.btn_verifikasi dengan nama variabel btnVerifikasi
  @BindView(R.id.btn_verifikasi) Button btnVerifikasi;

  //baris ini -> deklarasi variabel Komentar dengan nama 'komentar'
  private Komentar komentar;

  //baris ini -> method bawaan AppCompatActivity yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override protected void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk mengatur tampilan dalam kelas ini dari layout R.layout.activity_detail2
    setContentView(R.layout.activity_detail2);
    //baris ini -> statement untuk memasang ButterKnife di kelas ini
    ButterKnife.bind(this);
    //baris ini -> statement untuk memasang toolbar pada kelas ini
    setSupportActionBar(toolbar);
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    setTitle("Detail Komentar");
    //baris ini -> statement untuk inisiasi variabel ActionBar dengan nama 'actionBar'
    ActionBar actionBar = getSupportActionBar();
    //baris ini -> statement untuk mengecek apakah variabel actionBar telah di inisiasi dan set home display ditampilkan
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
    //baris ini -> statement untuk mengambil data komentar dari database berdasarkan id nya
    KomentarService.getInstance(this).get(this, new Komentar(getIntent().getIntExtra("id", 0)));
  }

  //baris ini -> method bawaan AppCompatActivity yang berfungsi untuk inisiasi menu di toolbar
  @Override public boolean onCreateOptionsMenu(Menu menu) {
    //baris ini -> inisiasi menu dengan menggunakan menu dengan id R.menu.detail_komentar
    getMenuInflater().inflate(R.menu.detail_komentar, menu);
    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onCreateOptionsMenu(menu);
  }

  //baris ini -> method bawaan AppCompatActivity yang berfungsi untuk menedeteksi menu yang dipilih oleh user
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id android.R.id.home lalu mengakhiri activity ini
    if (item.getItemId() == android.R.id.home) finish();

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_hapus
    if (item.getItemId() == R.id.action_hapus) {
      //baris ini -> statement untuk menampilkan dialog
      new AlertDialog.Builder(DetailActivity.this)
          //baris ini -> statement untuk memberikan isi konten dialog
          .setMessage("Anda akan menghapus komentar ini, lanjutkan?")
          //baris ini -> statement untuk mendeteksi jika user memilih button Ya
          .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
              //baris ini -> statement untuk menghapus data komentar di database berdasarkan id nya
              KomentarService.getInstance(DetailActivity.this)
                  .delete(DetailActivity.this, komentar);
            }
          })
          //baris ini -> statement untuk mendeteksi jika user memilih button Tidak
          .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
            }
          })
          //baris ini -> statement untuk menampilkan dialog
          .show();
    }

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_edit
    if (item.getItemId() == R.id.action_edit) {
      //baris ini -> statement untuk memulai activity baru (FormActivity) dengan membawa data id tanaman dari kelas ini
      startActivity(new Intent(this, FormActivity.class).putExtra("id", komentar.getId()));
    }

    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> method onVerifikasiClickListener dengan anotasi @OnClick dari ButterKnife yang berfungsi untuk mendeteksi button verifikasi yang ada ditampilan ini
  @OnClick(R.id.btn_verifikasi) public void onVerifikasiClick(View view) {
    //baris ini -> statement untuk mengecek apakah variabel komentar telah di inisiasi
    if (komentar != null) {
      //baris ini -> statement untuk set nilai status pada komentar menjadi true
      komentar.setStatus(true);
      //baris ini -> statement untuk mengupdate data komentar kedalam database berdasarkan id nya yang bertujuan untuk verifikasi data
      KomentarService.getInstance(this).saveOrUpdate(this, komentar);
    }
  }

  //baris ini -> method bawaan GetView dari KomentarService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onSuccess(Komentar komentar, String message) {
    //baris ini -> statement untuk mengecek apakah variabel komentar telah di inisiasi
    if (komentar != null) {
      //baris ini -> inisiasi variabel Komentar dengan data yang di dapat dari database
      this.komentar = komentar;
      //baris ini -> inisiasi variabel string dengan nama 'status' dengan isian tergantung dari apakah komentar sudah di verifikasi atau belum
      //berfungsi untuk memberi tahu user jika komentar sudah diverifikasi atau belum
      String status = komentar.isStatus() ? "Sudah terverifikasi" : "Belum Terverifikasi";
      //baris ini -> inisiasi variabel string dengan nama 'color' dengan isian tergantung dari apakah komentar sudah di verifikasi atau belum
      //berfungsi untuk memberikan warna pada tulisan jika komentar sudah di verifikasi atau belum
      String color = komentar.isStatus() ? "#1976D2" : "#d32f2f";
      //baris ini -> inisiasi variabel string dengan nama 'html' dengan isian dari gabungan semua field yang ada pada komentar
      String html = "<b>Nama : </b><br/>"
          + komentar.getNama()
          + "<br/><br/>"
          + "<b>Gejala : </b><br/>"
          + komentar.getGejala()
          + "<br/><br/>"
          + "<b>Perawatan : </b><br/>"
          + komentar.getPerawatan()
          + "<br/><br/>"
          + "<b>Dari : </b><br/>"
          + komentar.getNamaPengirim()
          + " ("
          + komentar.getEmailPengirim()
          + ")"
          + "<br/><br/>"
          + "<b>Nama Tanaman : </b><br/>"
          + komentar.getTanaman().getNama()
          + "<br/><br/>"
          + "<b>Status : </b><br/>"
          + "<b><font color=\""
          + color
          + "\">"
          + status
          + "</font></b>";
      //baris ini -> statement untuk menampilkan isian kedalam textview
      tvFirst.setText(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? Html.fromHtml(html,
          Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(html));
      //baris ini -> statement untuk menentukan apakah variabel btnVerifikasi akan ditampilkan atau tidak tergantung dari user apakah sudah login atau belum
      btnVerifikasi.setVisibility(komentar.isStatus() ? View.GONE : View.VISIBLE);
    } else {
      //baris ini -> statement untuk menampilkan toast (peringatan)
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
      //baris ini -> statement untuk mengakhiri activity ini
      finish();
    }
  }

  //baris ini -> method bawaan SaveView, DeleteView dari KomentarService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onSuccess(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk mengakhiri activity ini
    finish();
  }

  //baris ini -> method bawaan semua view dari KomentarService yang dieksekusi jika terjadi suatu kesalahan saat berhubungan dengan database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }
}
