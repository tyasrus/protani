package com.irvandha.protani.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.irvandha.protani.R;
import com.irvandha.protani.model.User;
import com.irvandha.protani.service.UserService;
import com.irvandha.protani.util.UserUtil;

/**
 * kelas ini berfungsi sebagai tampilan login
 */
public class LoginActivity extends AppCompatActivity implements UserService.LoginView {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Toolbar dengan id R.id.toolbar dengan nama variabel toolbar
  @BindView(R.id.toolbar) Toolbar toolbar;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi ImageView dengan id R.id.img dengan nama variabel img
  @BindView(R.id.img) ImageView img;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_title dengan nama variabel tvTitle
  @BindView(R.id.tv_title) TextView tvTitle;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi EditText dengan id R.id.username dengan nama variabel username
  @BindView(R.id.username) EditText username;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi EditText dengan id R.id.password dengan nama variabel password
  @BindView(R.id.password) EditText password;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextInputLayout dengan id R.id.textInputLayout dengan nama variabel textInputLayout
  @BindView(R.id.textInputLayout) TextInputLayout textInputLayout;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Button dengan id R.id.btn_masuk dengan nama variabel btnMasuk
  @BindView(R.id.btn_masuk) Button btnMasuk;

  //baris ini -> method bawaan AppCompatActivity yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override protected void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk mengatur tampilan dalam kelas ini dari layout R.layout.activity_main
    setContentView(R.layout.activity_login);
    //baris ini -> statement untuk memasang ButterKnife di kelas ini
    ButterKnife.bind(this);
    //baris ini -> statement untuk memasang toolbar pada kelas ini
    setSupportActionBar(toolbar);
    //baris ini -> statement untuk inisiasi variabel ActionBar dengan nama 'actionBar'
    ActionBar actionBar = getSupportActionBar();
    //baris ini -> statement untuk mengecek apakah variabel actionBar telah di inisiasi dan set home display ditampilkan
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
  }

  //baris ini -> method bawaan AppCompatActivity yang berfungsi untuk mendeteksi menu yang dipilih oleh user
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id android.R.id.home lalu mengakhiri activity ini
    if (item.getItemId() == android.R.id.home) finish();
    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> method onLoginButtonClick dengan anotasi @OnClick dari ButterKnife yang berfungsi untuk mendeteksi button login
  @OnClick({ R.id.btn_masuk }) public void onLoginButtonClick(View view) {
    //baris ini -> statement jika user menekan button dengan id R.id.btn_masuk
    if (view.getId() == R.id.btn_masuk) {
      //baris ini -> statement apakah variabel username kosong
      if (username.getText().toString().trim().length() == 0) {
        //baris ini -> statement untuk memberi error ke variabel username
        username.setError("Username masih kosong");
        //baris ini -> statement apakah variabel password kosong
      } else if (password.getText().toString().trim().length() == 0) {
        //baris ini -> statement untuk memberi error ke variabel password
        password.setError("Password masih kosong");
      } else {
        //baris ini -> statement untuk inisiasi User dengan nama 'user'
        User user = new User();
        //baris ini -> statement untuk set name dari variabel user
        user.setName(username.getText().toString());
        //baris ini -> statement untuk set password dari variabel user
        user.setPassword(password.getText().toString());
        //baris ini -> statement untuk melakukan login dari kelas UserService
        UserService.getInstance(this).login(this, user);
        //baris ini -> statement untuk eksekusi method send dengan 1 parameter bernilai true
        send(true);
      }
    }
  }

  //baris ini -> method bawaan LoginView dari UserService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onSuccess(User user, String message) {
    //baris ini -> statement untuk membuat session ketika user berhasil login
    UserUtil.getInstance(this).setLoginState(user);
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk mengakhiri activity ini
    finish();
  }

  //baris ini -> method bawaan LoginView dari UserService yang dieksekusi jika data gagal di simpan dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk eksekusi method send dengan 1 parameter bernilai false
    send(false);
  }

  //baris ini -> inisiasi method dengan nama 'send' dengan parameter boolean dengan nama 'isSending'
  private void send(boolean isSending) {
    //baris ini -> statement untuk membuat variabel username bisa di edit atau tidak tergantung parameter isSending
    username.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel password bisa di edit atau tidak tergantung parameter isSending
    password.setEnabled(!isSending);
    //baris ini -> statement untuk set background variabel btnMasuk tergantung dari parameter issending
    btnMasuk.setBackgroundResource(isSending ? R.drawable.bg_button_loading : R.color.colorAccent);
    //baris ini -> statement untuk set text variabel btnMasuk tergantung dari parameter issending
    btnMasuk.setText(isSending ? "Tunggu Sebentar ..." : "Masuk");
    //baris ini -> statement untuk set warna teks variabel btnMasuk tergantung dari parameter issending
    btnMasuk.setTextColor(
        isSending ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.colorAccent, null) : getResources().getColor(R.color.colorAccent)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
                android.R.color.white, null) : getResources().getColor(android.R.color.white));
  }
}
