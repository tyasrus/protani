package com.irvandha.protani.ui.tanaman;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.JenisKomentarService;
import com.irvandha.protani.service.TanamanService;
import com.irvandha.protani.util.UserUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * kelas ini berfungsi sebagai tampilan tambah tanaman atau update tanaman
 */
public class FormActivity extends AppCompatActivity
    implements TanamanService.GetView, TanamanService.SaveView, JenisKomentarService.AllJenisView {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.textInputLayout2) TextInputLayout textInputLayout2;
  @BindView(R.id.textInputLayout3) TextInputLayout textInputLayout3;
  @BindView(R.id.fab) FloatingActionButton fab;
  @BindView(R.id.nama_tanaman) EditText namaTanaman;
  @BindView(R.id.deskripsi_tanaman) EditText deskripsiTanaman;
  @BindView(R.id.nama_komentar) EditText namaKomentar;
  @BindView(R.id.tv_spinner) TextView tvSpinner;
  @BindView(R.id.spinner_jenis) Spinner spinnerJenis;
  @BindView(R.id.gejala) EditText gejala;
  @BindView(R.id.perawatan) EditText perawatan;
  @BindView(R.id.nama_pengirim) EditText namaPengirim;
  @BindView(R.id.email) EditText email;
  @BindView(R.id.tv_title_hama_penyakit) TextView tvTitleHamaPenyakit;

  //baris ini -> deklarasi variabel Tanaman dengan nama 'tanaman'
  private Tanaman tanaman;
  private List<Komentar.Jenis> jenisList;

  //baris ini -> method bawaan AppCompatActivity yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override protected void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk mengatur tampilan dalam kelas ini dari layout R.layout.activity_form
    setContentView(R.layout.activity_form);
    //baris ini -> statement untuk memasang ButterKnife di kelas ini
    ButterKnife.bind(this);
    //baris ini -> statement untuk memasang toolbar pada kelas ini
    setSupportActionBar(toolbar);
    //baris ini -> statement untuk inisiasi variabel ActionBar dengan nama 'actionBar'
    ActionBar actionBar = getSupportActionBar();
    //baris ini -> statement untuk mengecek apakah variabel actionBar telah di inisiasi dan set home display ditampilkan
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    setTitle("Tambah Tanaman");

    //baris ini -> statement untuk mengecek apakah ada data id dari activity atau fragment sebelumnya
    if (getIntent() != null && getIntent().getIntExtra("id", 0) != 0) {
      //baris ini -> statement untuk mengambil data tanaman dari database berdasarkan id nya
      TanamanService.getInstance(this).get(this, new Tanaman(getIntent().getIntExtra("id", 0)));
      tvTitleHamaPenyakit.setVisibility(View.GONE);
      namaKomentar.setVisibility(View.GONE);
      gejala.setVisibility(View.GONE);
      perawatan.setVisibility(View.GONE);
      spinnerJenis.setVisibility(View.GONE);
      tvSpinner.setVisibility(View.GONE);
      namaPengirim.setVisibility(View.GONE);
      email.setVisibility(View.GONE);
    }

    JenisKomentarService.getInstance(this).getAll(this);
  }

  //baris ini -> method onFormClickListener dengan anotasi @OnClick dari ButterKnife yang berfungsi untuk mendeteksi button simpan
  @OnClick(R.id.fab) public void onFormClickListener(View view) {
    //baris ini -> statement apakah variabel nama kosong
    if (namaTanaman.getText().toString().trim().length() == 0) {
      //baris ini -> statement untuk memberi error ke variabel nama
      namaTanaman.setError("Nama masih kosong");
      //baris ini -> statement apakah variabel deskripsi kosong
    } else if (deskripsiTanaman.getText().toString().trim().length() == 0) {
      //baris ini -> statement untuk memberi error ke variabel deskripsi
      deskripsiTanaman.setError("Deskripsi masih kosong");
    } else {
      //baris ini -> statement untuk mengecek apakah variabel object tanaman null
      if (tanaman == null) {
        //baris ini -> statement untuk inisiasi Tanaman dengan nama 'tanaman'
        tanaman = new Tanaman();
        //baris ini -> statement untuk set nama dari variabel tanaman
        tanaman.setNama(namaTanaman.getText().toString());
        //baris ini -> statement untuk set deskripsi dari variabel tanaman
        tanaman.setDeskripsi(deskripsiTanaman.getText().toString());
        //baris ini -> statement untuk set status dari variabel user yang tergantung dari apakah admin sudah login
        tanaman.setStatus(UserUtil.getInstance(this).isLogin());
      } else {
        //baris ini -> statement untuk set nama dari variabel tanaman
        tanaman.setNama(namaTanaman.getText().toString());
        //baris ini -> statement untuk set deskripsi dari variabel tanaman
        tanaman.setDeskripsi(deskripsiTanaman.getText().toString());
        //baris ini -> statement untuk set status dari variabel user yang tergantung dari apakah admin sudah login
        //tanaman.setStatus(false);
      }

      if (namaKomentar.getText().toString().trim().length() > 0) {
        if (gejala.getText().toString().trim().length() == 0) {
          //baris ini -> statement untuk memberi error ke variabel deskripsi
          gejala.setError("Gejala masih kosong");
        } else if (perawatan.getText().toString().trim().length() == 0) {
          //baris ini -> statement untuk memberi error ke variabel deskripsi
          perawatan.setError("Perawatan masih kosong");
        } else if (spinnerJenis.getSelectedItemPosition() == 0) {
          //baris ini -> statement untuk memberi error ke variabel tvSpinner
          tvSpinner.setError("Pilih salah satu jenis hama disamping");
          //baris ini -> statement apakah variabel namaPengirim kosong
        } else if (namaPengirim.getText().toString().trim().length() == 0) {
          //baris ini -> statement untuk memberi error ke variabel deskripsi
          namaPengirim.setError("Nama pengirim masih kosong");
        } else if (email.getText().toString().trim().length() == 0) {
          //baris ini -> statement untuk memberi error ke variabel deskripsi
          email.setError("Email pengirim masih kosong");
        } else {
          Komentar komentar = new Komentar();
          //baris ini -> statement untuk set nama pengirim dari variabel komentar
          komentar.setNamaPengirim(namaPengirim.getText().toString());
          //baris ini -> statement untuk set email pengirim dari variabel komentar
          komentar.setEmailPengirim(email.getText().toString());
          //baris ini -> statement untuk set status dari variabel komentar
          komentar.setStatus(UserUtil.getInstance(this).isLogin());
          //baris ini -> statement untuk set nama dari variabel komentar
          komentar.setNama(namaKomentar.getText().toString());
          //baris ini -> statement untuk set gejala dari variabel komentar
          komentar.setGejala(gejala.getText().toString());
          //baris ini -> statement untuk set perawatan dari variabel komentar
          komentar.setPerawatan(perawatan.getText().toString());
          //baris ini -> statement untuk set object jenis dari variabel komentar
          komentar.setJenis(jenisList.get(spinnerJenis.getSelectedItemPosition() - 1));
          komentar.setTanaman(tanaman);

          TanamanService.getInstance(this).saveWithKomentar(this, komentar);
        }
      } else {
        //baris ini -> statement untuk menyimpan atau memperbarui data tanaman dari kelas TanamanService
        //method ini akan menyimpan data tanaman baru jika id = 0
        //method ini akan memperbarui data tanaman jika id > 0
        TanamanService.getInstance(this).saveOrUpdate(this, tanaman);
      }

      //baris ini -> statement untuk eksekusi method send dengan 1 parameter bernilai true
      send(true);
    }
  }

  //baris ini -> method bawaan AppCompatActivity yang berfungsi untuk menedeteksi menu yang dipilih oleh user
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id android.R.id.home lalu mengakhiri activity ini
    if (item.getItemId() == android.R.id.home) finish();
    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> inisiasi method dengan nama 'send' dengan parameter boolean dengan nama 'isSending'
  private void send(boolean isSending) {
    //baris ini -> statement untuk membuat variabel nama bisa di edit atau tidak tergantung parameter isSending
    namaTanaman.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel deskripsi bisa di edit atau tidak tergantung parameter isSending
    deskripsiTanaman.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel fab bisa di tekan atau tidak tergantung parameter isSending
    fab.setEnabled(!isSending);
  }

  //baris ini -> method bawaan GetView dari TanamanService yang dieksekusi jika data berhasil di ambil dari database
  @Override public void onSuccess(Tanaman tanaman, String message) {
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    setTitle("Edit Tanaman");
    //baris ini -> statement untuk set variabel tanaman dengan data yang didapat dari database
    this.tanaman = tanaman;
    //baris ini -> statement untuk mengecek apakah tanaman tidak kosong
    if (tanaman != null) {
      //baris ini -> statement untuk set nilai variabel nama dengan nama tanaman
      namaTanaman.setText(tanaman.getNama());
      //baris ini -> statement untuk set nilai variabel deskripsi dengan deskripsi tanaman
      deskripsiTanaman.setText(tanaman.getDeskripsi());
    } else {
      //baris ini -> statement untuk menampilkan toast (peringatan)
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
      //baris ini -> statement untuk mengakhiri activity ini
      finish();
    }
  }

  //baris ini -> method bawaan SaveView dari TanamanService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onSuccess(String message) {
    String finalMessage = UserUtil.getInstance(this).isLogin() ? message
        : "Tanaman sukses ditambahkan, admin akan memverifikasi data anda terlebih dahulu";
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, finalMessage, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk mengakhiri activity ini
    finish();
  }

  @Override public void onSuccess(List<Komentar.Jenis> jenis, String message) {
    //baris ini -> statement untuk set variabel array jenisList dengan data yang didapat dari database
    jenisList = jenis;
    //baris ini -> statement untuk inisiasi variabel array String dengan nama 'strings'
    List<String> strings = new ArrayList<>();
    //baris ini -> statement untuk menambah item ke dalam variabel strings
    strings.add("-");
    //baris ini -> statement untuk perulangan item dari hasil semua data yang didapat dari database
    for (Komentar.Jenis item : jenis) {
      //baris ini -> statement untuk menambah nama dari item ke dalam variabel strings
      strings.add(item.getNama());
    }

    //baris ini -> statement untuk set adapter ke variabel spinnerJenis untuk menampilkan item jenis komentar
    spinnerJenis.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, strings));
  }

  //baris ini -> method bawaan SaveView, GetView dari TanamanService yang dieksekusi jika data gagal di simpan atau di ambil dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this,
        message.trim().length() > 0 ? message : "Terjadi kesalahan saat mengakses server",
        Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk eksekusi method send dengan 1 parameter bernilai false
    send(false);
  }
}
