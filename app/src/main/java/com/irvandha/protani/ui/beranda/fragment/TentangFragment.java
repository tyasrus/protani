package com.irvandha.protani.ui.beranda.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.irvandha.protani.BuildConfig;
import com.irvandha.protani.R;

/**
 * kelas ini berfungsi sebagai tampilan tentang pembuat aplikasi
 */
public class TentangFragment extends Fragment {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_tentang dengan nama variabel tvTentang
  @BindView(R.id.tv_tentang) TextView tvTentang;
  //baris ini -> inisiasi variabel Unbinder dengan nama unbinder yang berfungsi untuk melepaskan pendeteksian view ketika kelas fragment ini selesai digunakan
  Unbinder unbinder;

  //baris ini -> method bawaan Fragment yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    //baris ini -> inisiasi variabel view dari layout dengan id R.id.fragment_komentar
    View view = inflater.inflate(R.layout.fragment_tentang, container, false);
    //baris ini -> inisiasi variabel unbinder sebagai instance dari kelas ButterKnife
    unbinder = ButterKnife.bind(this, view);
    tvTentang.setText(
        "Aplikasi ini dibuat untuk memberikan informasi kepada pengguna mengenai hama pada tanaman pangan dan cara perawatan tanaman yang terkena hama\n\nDikembangkan oleh Irvandha Widi Pratama\n\n\n\nVersi aplikasi : "
            .concat(BuildConfig.VERSION_NAME));
    return view;
  }

  //baris ini -> method bawaan Fragment yang dieksekusi jika fragment ini digunakan lagi
  @Override public void onResume() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onResume();
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    getActivity().setTitle("Tentang Aplikasi");
  }

  //baris ini -> method bawaan Fragment yang dieksekusi jika fragment telah berhenti
  @Override public void onDestroyView() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onDestroyView();
    //baris ini -> statement untuk melepaskan semua view yang di identifikasi pafa fragment ini
    unbinder.unbind();
  }
}
