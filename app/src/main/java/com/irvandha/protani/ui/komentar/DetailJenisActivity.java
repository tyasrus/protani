package com.irvandha.protani.ui.komentar;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.service.KomentarService;
import com.irvandha.protani.util.UserUtil;

public class DetailJenisActivity extends AppCompatActivity implements KomentarService.GetView {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.tv_first) TextView tvFirst;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_jenis);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    ActionBar actionBar = getSupportActionBar();
    //baris ini -> statement untuk mengecek apakah variabel actionBar telah di inisiasi dan set home display ditampilkan
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

    String judul = getIntent().getStringExtra("judul");
    setTitle("Detail " + judul.toUpperCase().substring(0, 1) + judul.substring(1));

    Komentar komentar = new Komentar();
    komentar.setId(getIntent().getIntExtra("id", 0));
    KomentarService.getInstance(this).get(this,komentar);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id android.R.id.home lalu mengakhiri activity ini
    if (item.getItemId() == android.R.id.home) finish();
    return super.onOptionsItemSelected(item);
  }

  @Override public void onSuccess(Komentar komentar, String message) {
    String html = "<b>Nama : </b><br/>"
        + komentar.getNama()
        + "<br/><br/>"
        + "<b>Gejala : </b><br/>"
        + komentar.getGejala()
        + "<br/><br/>"
        + "<b>Perawatan : </b><br/>"
        + komentar.getPerawatan()
        + "<br/><br/>"
        + "<b>Dari : </b><br/>"
        + komentar.getNamaPengirim()
        + " ("
        + komentar.getEmailPengirim()
        + ")";

    tvFirst.setText(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? Html.fromHtml(html,
        Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(html));
  }

  @Override public void onFailed(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }
}
