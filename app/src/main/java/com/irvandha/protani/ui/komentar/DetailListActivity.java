package com.irvandha.protani.ui.komentar;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.KomentarService;
import com.irvandha.protani.ui.adapter.OneItemAdapter;
import java.util.ArrayList;
import java.util.List;

public class DetailListActivity extends AppCompatActivity
    implements KomentarService.AllView, OneItemAdapter.OnItemClickListener {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.tv_announcer) TextView tvAnnouncer;
  @BindView(R.id.recycler) RecyclerView recycler;

  //baris ini -> deklarasi variabel String dengan nama 'ID_JENIS_KOMENTAR' yang berfungsi untuk sebagai penanda id jenis komentar
  public static final String ID_JENIS_KOMENTAR = "id_jenis_hama";
  //baris ini -> deklarasi variabel String dengan nama 'ID_TANAMAN' yang berfungsi untuk penanda id tanaman
  public static final String ID_TANAMAN = "id_tanaman";

  //baris ini -> deklarasi variabel array dengan nama 'komentars'
  private List<Komentar> komentars;
  //baris ini -> deklarasi variabel OneItemAdapter dengan nama 'adapter'
  private OneItemAdapter adapter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_list);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    String judul = getIntent().getStringExtra("judul");
    setTitle("Daftar " + judul.toUpperCase().substring(0, 1) + judul.substring(1));
    //baris ini -> statement untuk inisiasi variabel ActionBar dengan nama 'actionBar'
    ActionBar actionBar = getSupportActionBar();
    //baris ini -> statement untuk mengecek apakah variabel actionBar telah di inisiasi dan set home display ditampilkan
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
    //baris ini -> inisiasi variabel komentars
    komentars = new ArrayList<>();

    //baris ini -> inisiasi variabel adapter
    adapter = new OneItemAdapter();
    //baris ini -> memberikan nilai ke pada method setter komentars pada adapter tersebut
    adapter.setKomentars(komentars);
    //baris ini -> memberikan nilai ke pada method setter isOnlyTitle untuk menampilkan judul saja
    adapter.setOnlyTitle(true);
    //baris ini -> memberikan nilai ke pada method setter listener untuk mendeteksi klik pada tampilan
    adapter.setOnItemClickListener(this);

    //baris ini -> memberikan penugasan layout manager ke pada method setter layout manager pada recyclerView
    recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    //baris ini -> memberikan penugasan adapter ke pada method setter adapter pada recyclerView
    recycler.setAdapter(adapter);

    //baris ini -> inisiasi variabel Komentar dengan nama 'komentar'
    Komentar komentar = new Komentar();
    //baris ini -> statement untuk set object jenis ke variabel komentar
    komentar.setJenis(new Komentar.Jenis(getIntent().getIntExtra(ID_JENIS_KOMENTAR, 0)));
    //baris ini -> statement untuk set object tanaman ke variabel komentar
    komentar.setTanaman(new Tanaman(getIntent().getIntExtra(ID_TANAMAN, 0)));

    //baris ini -> statement untuk mendapatkan semua data dari koemntar yang ada dalam database
    KomentarService.getInstance(this).getAll(this, komentar);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id android.R.id.home lalu mengakhiri activity ini
    if (item.getItemId() == android.R.id.home) finish();
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> method bawaan AllView dari KomentarService yang dieksekusi jika data gagal di ambil dari database
  @Override public void onSuccess(List<Komentar> komentars, String message) {
    //baris ini -> statement untuk mengecek apakah variabel komentars telah di inisiasi dan tidak kosong
    if (komentars != null && !komentars.isEmpty()) {
      //baris ini -> statement untuk menambahkan semua data dari database ke dalam variabel komentars
      this.komentars.addAll(komentars);
      //baris ini -> statement menyegarkan tampilan jika data berhasil dari database
      adapter.notifyDataSetChanged();
      //baris ini -> statement untuk menyembunyikan textview
      tvAnnouncer.setVisibility(View.GONE);
    } else {
      //baris ini -> statement memberikan nilai ke tvAnnouncer
      tvAnnouncer.setText("Belum ada data");
    }
  }

  //baris ini -> method bawaan AllView dari KomentarService yang dieksekusi jika data gagal di ambil dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement memberikan nilai ke tvAnnouncer
    tvAnnouncer.setText("Belum ada data");
  }

  @Override public void onItemClick(View view, int position) {
    startActivity(new Intent(this, DetailJenisActivity.class).putExtra("id",
        komentars.get(position).getId()).putExtra("judul", getIntent().getStringExtra("judul")));
  }

  @Override public void onLongItemClick(View view, int position) {

  }
}
