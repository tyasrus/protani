package com.irvandha.protani.ui.tanaman;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.JenisKomentarService;
import com.irvandha.protani.service.TanamanService;
import com.irvandha.protani.ui.adapter.OneItemAdapter;
import com.irvandha.protani.ui.komentar.DetailListActivity;
import com.irvandha.protani.ui.komentar.fragment.DaftarFragment;
import com.irvandha.protani.util.UserUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * kelas ini berfungsi sebagai tampilan detail komentar sesuai dengan jenis komentar
 * kelas ini juga berfungsi sebagai tampilan semua komentar pada tampilan detail tanaman
 */
public class DetailActivity extends AppCompatActivity
    implements JenisKomentarService.AllJenisView, TanamanService.GetView, TanamanService.SaveView,
    TanamanService.DeleteView, OneItemAdapter.OnItemClickListener {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Toolbar dengan id R.id.toolbar dengan nama variabel toolbar
  @BindView(R.id.toolbar) Toolbar toolbar;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi AppBarLayout dengan id R.id.app_bar dengan nama variabel appBar
  @BindView(R.id.app_bar) AppBarLayout appBar;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_deskripsi dengan nama variabel tvDeskripsi
  @BindView(R.id.tv_deskripsi) TextView tvDeskripsi;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TabLayout dengan id R.id.tab dengan nama variabel tab
  //@BindView(R.id.tab) TabLayout tab;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi ViewPager dengan id R.id.pager dengan nama variabel pager
  //@BindView(R.id.pager) ViewPager pager;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Button dengan id R.id.btn_verifikasi dengan nama variabel btnVerifikasi
  @BindView(R.id.btn_verifikasi) Button btnVerifikasi;
  @BindView(R.id.recycler) RecyclerView recycler;

  //baris ini -> deklarasi variabel Tanaman dengan nama 'tanaman'
  private Tanaman tanaman;
  private List<Komentar.Jenis> jenisKomentars;
  private OneItemAdapter adapter;

  //baris ini -> method bawaan AppCompatActivity yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override protected void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk mengatur tampilan dalam kelas ini dari layout R.layout.activity_detail
    setContentView(R.layout.activity_detail);
    //baris ini -> statement untuk memasang ButterKnife di kelas ini
    ButterKnife.bind(this);
    //baris ini -> statement untuk memasang toolbar pada kelas ini
    setSupportActionBar(toolbar);
    //baris ini -> statement untuk inisiasi variabel ActionBar dengan nama 'actionBar'
    ActionBar actionBar = getSupportActionBar();
    //baris ini -> statement untuk mengecek apakah variabel actionBar telah di inisiasi dan set home display ditampilkan
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
    jenisKomentars = new ArrayList<>();
    adapter = new OneItemAdapter();
    adapter.setJenisKomentars(jenisKomentars);
    adapter.setOnItemClickListener(this);
    recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    recycler.setAdapter(adapter);
  }

  //baris ini -> method bawaan AppCompatActivity yang dieksekusi ketika activity ini sudah tampil
  @Override protected void onStart() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onStart();
    //baris ini -> inisiasi variabel int (integer) dengan nama 'id' yang berisi data dari activity atau fragment sebelumnya
    int id = getIntent().getIntExtra("id", 0);
    //baris ini -> statement untuk mengecek apakah variabel id tidak sama dengan 0
    if (id != 0 && jenisKomentars.size() == 0) {
      //baris ini -> statement untuk mendapatkan data dari tanaman yang ada dalam database berdasarkan id nya
      TanamanService.getInstance(this).get(this, new Tanaman(id));
    }
  }

  //baris ini -> method onVerifikasiClickListener dengan anotasi @OnClick dari ButterKnife yang berfungsi untuk mendeteksi button verifikasi yang ada ditampilan ini
  @OnClick(R.id.btn_verifikasi) public void onVerifikasiClickListener(View view) {
    //baris ini -> statement untuk set nilai status pada tanaman menjadi true
    tanaman.setStatus(true);
    //baris ini -> statement untuk mengupdate data tanaman kedalam database berdasarkan id nya yang bertujuan untuk verifikasi data
    TanamanService.getInstance(this).saveOrUpdate(this, tanaman);
  }

  //baris ini -> method bawaan AppCompatActivity yang berfungsi untuk inisiasi menu di toolbar
  @Override public boolean onCreateOptionsMenu(Menu menu) {
    //baris ini -> inisiasi menu dengan menggunakan menu dengan id R.menu.detail_tanaman
    getMenuInflater().inflate(R.menu.detail_tanaman, menu);
    //baris ini -> statement untuk identifikasi item dengan id R.id.action_edit lalu akan tampil jika admin telah masuk, jika belum maka tidak tampil
    menu.findItem(R.id.action_edit).setVisible(UserUtil.getInstance(this).isLogin());
    //baris ini -> statement untuk identifikasi item dengan id R.id.action_hapus lalu akan tampil jika admin telah masuk, jika belum maka tidak tampil
    menu.findItem(R.id.action_hapus).setVisible(UserUtil.getInstance(this).isLogin());
    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onCreateOptionsMenu(menu);
  }

  //baris ini -> method bawaan AppCompatActivity yang berfungsi untuk menedeteksi menu yang dipilih oleh user
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id android.R.id.home lalu mengakhiri activity ini
    if (item.getItemId() == android.R.id.home) finish();

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_tambah
    if (item.getItemId() == R.id.action_tambah) {
      //baris ini -> statement untuk memulai activity baru (FormActivity) dengan membawa data id tanaman dari kelas ini
      startActivity(new Intent(this, com.irvandha.protani.ui.komentar.FormActivity.class).putExtra(
          "id_tanaman", tanaman.getId()));
    }

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_edit
    if (item.getItemId() == R.id.action_edit) {
      //baris ini -> statement untuk memulai activity baru (FormActivity) dengan membawa data id tanaman dari kelas ini
      startActivity(new Intent(this, FormActivity.class).putExtra("id", tanaman.getId()));
    }

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_hapus
    if (item.getItemId() == R.id.action_hapus) {
      //baris ini -> statement untuk menampilkan dialog
      new AlertDialog.Builder(DetailActivity.this)
          //baris ini -> statement untuk memberikan isi konten dialog
          .setMessage("Anda akan menghapus tanaman ini, lanjutkan?")
          //baris ini -> statement untuk mendeteksi jika user memilih button Ya
          .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
              //baris ini -> statement untuk menghapus data tanaman di database berdasarkan id nya
              TanamanService.getInstance(DetailActivity.this).delete(DetailActivity.this, tanaman);
            }
          })
          //baris ini -> statement untuk mendeteksi jika user memilih button Tidak
          .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
            }
          })
          //baris ini -> statement untuk menampilkan dialog
          .show();
    }

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_refresh
    if (item.getItemId() == R.id.action_refresh) {
      //baris ini -> statement untuk menampilkan dialog
      new AlertDialog.Builder(this)
          //baris ini -> statement untuk memberikan isi konten dialog
          .setMessage("Anda akan merefresh halaman ini, lanjutkan?")
          //baris ini -> statement untuk mendeteksi jika user memilih button Ya
          .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
              //baris ini -> statement untuk mengambil data tanaman dari database berdasarkan id nya
              TanamanService.getInstance(DetailActivity.this)
                  .get(DetailActivity.this,
                      tanaman != null ? tanaman : new Tanaman(getIntent().getIntExtra("id", 0)));
            }
          })
          //baris ini -> statement untuk mendeteksi jika user memilih button Tidak
          .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
            }
          })
          //baris ini -> statement untuk menampilkan dialog
          .show();
    }

    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> method bawaan GetView dari TanamanService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onSuccess(Tanaman tanaman, String message) {
    //baris ini -> statement untuk set variabel tanaman dengan data yang didapat dari database
    this.tanaman = tanaman;
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    setTitle(tanaman.getNama());
    //baris ini -> statement untuk set teks ke dalam variabel tvDeskripsi
    tvDeskripsi.setText(tanaman.getDeskripsi());
    //baris ini -> statement untuk set tampilan button verifikasi akan ditampilkan atau tidak tergantung dari status tanaman
    btnVerifikasi.setVisibility(tanaman.isStatus() ? View.GONE : View.VISIBLE);
    //baris ini -> statement untuk mengecek status tanaman apakah true
    if (tanaman.isStatus()) {
      //baris ini -> statement untuk mengambil semua data jenis komentar dari database
      JenisKomentarService.getInstance(this).getAll(this);
    }
  }

  //baris ini -> method bawaan AllJenisView dari JenisKomentarService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onSuccess(List<Komentar.Jenis> jenis, String message) {
    //baris ini -> statement untuk set adapter untuk tampilan halam per tab jenis komentar pada detail tanaman
    //pager.setAdapter(new DetailPagerAdapter(getSupportFragmentManager(), jenis));
    //baris ini -> statement untuk set adapter untuk tampilan tab jenis komentar pada detail tanaman
    //tab.setupWithViewPager(pager);
    jenisKomentars.addAll(jenis);
    adapter.notifyDataSetChanged();
  }

  //baris ini -> method bawaan SaveView, DeleteView dari TanamanService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onSuccess(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk mengakhiri activity ini
    finish();
  }

  //baris ini -> method bawaan semua view dari TanamanService atau JenisKomentarService yang dieksekusi jika data berhasil di simpan dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override public void onItemClick(View view, int position) {
    startActivity(new Intent(this, DetailListActivity.class).putExtra(DetailListActivity.ID_TANAMAN,
        tanaman.getId())
        .putExtra(DetailListActivity.ID_JENIS_KOMENTAR, jenisKomentars.get(position).getId())
        .putExtra("judul", jenisKomentars.get(position).getNama()));
  }

  @Override public void onLongItemClick(View view, int position) {

  }
}
