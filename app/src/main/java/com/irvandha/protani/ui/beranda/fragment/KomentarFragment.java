package com.irvandha.protani.ui.beranda.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.service.KomentarService;
import com.irvandha.protani.ui.adapter.OneItemAdapter;
import com.irvandha.protani.ui.komentar.DetailActivity;
import java.util.ArrayList;
import java.util.List;

/**
 * kelas ini berfungsi sebagai tampilan list semua komentar untuk admin
 */
public class KomentarFragment extends Fragment
    implements KomentarService.AllView, OneItemAdapter.OnItemClickListener {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi RecyclerView dengan id R.id.recyclerView dengan nama variabel recyclerView
  @BindView(R.id.recyclerView) RecyclerView recyclerView;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_announcer dengan nama variabel tvAnnouncer
  @BindView(R.id.tv_announcer) TextView tvAnnouncer;
  //baris ini -> inisiasi variabel Unbinder dengan nama unbinder yang berfungsi untuk melepaskan pendeteksian view ketika kelas fragment ini selesai digunakan
  Unbinder unbinder;

  //baris ini -> deklarasi variabel array dengan nama komentars
  private List<Komentar> komentars;
  //baris ini -> deklarasi variabel OneItemAdapter dengan nama adapter
  private OneItemAdapter adapter;

  //baris ini -> method bawaan Fragment yang akan di eksekusi pertama kali
  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk menandakan bahwa kelas ini akan menggunakan menu di toolbar (tampilan kotak diatas)
    setHasOptionsMenu(true);
  }

  //baris ini -> method bawaan Fragment yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    //baris ini -> inisiasi variabel view dari layout dengan id R.id.fragment_komentar
    View view = inflater.inflate(R.layout.fragment_komentar, container, false);
    //baris ini -> inisiasi variabel unbinder sebagai instance dari kelas ButterKnife
    unbinder = ButterKnife.bind(this, view);
    //baris ini -> inisiasi variabel komentars
    komentars = new ArrayList<>();

    //baris ini -> inisiasi variabel adapter
    adapter = new OneItemAdapter(true);
    //baris ini -> memberikan nilai ke pada method setter komentars pada adapter tersebut
    adapter.setKomentars(komentars);
    //baris ini -> memberikan nilai ke pada method setter listener pada adapter tersebut
    adapter.setOnItemClickListener(this);

    //baris ini -> memberikan penugasan layout manager ke pada method setter layout manager pada recyclerView
    recyclerView.setLayoutManager(
        new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    //baris ini -> memberikan penugasan adapter ke pada method setter adapter pada recyclerView
    recyclerView.setAdapter(adapter);

    //baris ini -> statement untuk mendapatkan semua data dari koemntar yang ada dalam database
    KomentarService.getInstance(getActivity()).getAll(this, new Komentar());

    //baris ini -> statement untuk nilai kembali dengan menggunakan variabel view
    return view;
  }

  //baris ini -> method bawaan Fragment yang dieksekusi jika fragment ini digunakan lagi
  @Override public void onResume() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onResume();
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    getActivity().setTitle("Daftar Komentar");
  }

  //baris ini -> method bawaan Fragment yang dieksekusi jika fragment telah berhenti
  @Override public void onDestroyView() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onDestroyView();
    //baris ini -> statement untuk melepaskan semua view yang di identifikasi pafa fragment ini
    unbinder.unbind();
  }

  //baris ini -> method bawaan AllView dari KomentarService yang dieksekusi jika data berhasil di ambil dari database
  @Override public void onSuccess(List<Komentar> komentars, String message) {
    //baris ini -> statement untuk menambahkan semua data dari database ke dalam variabel komentars
    this.komentars.addAll(komentars);
    //baris ini -> statement menyegarkan tampilan jika data berhasil dari database
    adapter.notifyDataSetChanged();
    //baris ini -> statement untuk menyembunyikan textview
    tvAnnouncer.setVisibility(View.GONE);
  }

  //baris ini -> method bawaan AllView dari KomentarService yang dieksekusi jika data gagal di ambil dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement memberikan nilai ke tvAnnouncer
    tvAnnouncer.setText("Belum ada data");
  }

  //baris ini -> method bawaan Fragment yang berfungsi untuk inisiasi menu di toolbar
  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreateOptionsMenu(menu, inflater);
    //baris ini -> inisiasi menu dengan menggunakan menu dengan id R.menu.komentar
    inflater.inflate(R.menu.komentar, menu);
    //baris ini -> statement untuk menyembunyikan menu item dengan id R.id.action_bantuan
    menu.findItem(R.id.action_bantuan).setVisible(false);
  }

  //baris ini -> method bawaan Fragment yang berfungsi untuk menedeteksi menu yang dipilih oleh user
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id R.id.action_bantuan
    if (item.getItemId() == R.id.action_bantuan) {
      //baris ini -> statement untuk menampilkan dialog
      new AlertDialog.Builder(getActivity())
          //baris ini -> statement untuk memberikan judul dialog
          .setTitle("Bantuan Konfirmasi Komentar")
          //baris ini -> statement untuk memberikan isi konten dialog
          .setMessage(
              "Untuk mengkonfirmasi komentar, silahkan pilih komentar yang menurut anda valid, lalu pilih untuk masuk halaman detail, disana ada button untuk verifikasi komentar")
          //baris ini -> statement untuk mendeteksi jika user memilih button Mengerti
          .setPositiveButton("Mengerti", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
            }
          })
          //baris ini -> statement untuk menampilkan dialog
          .show();
    }

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_refresh
    if (item.getItemId() == R.id.action_refresh) {
      //baris ini -> statement untuk menampilkan dialog
      new AlertDialog.Builder(getActivity())
          //baris ini -> statement untuk memberikan isi konten dialog
          .setMessage(
          "Anda akan merefresh halaman ini, lanjutkan?")
          //baris ini -> statement untuk mendeteksi jika user memilih button Ya
          .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
              //baris ini -> statement untuk mengecek jika variabel komentars tidak kosong
              if (!komentars.isEmpty()) {
                //baris ini -> statement untuk menghapus semua item dalam variabel komentars
                komentars.clear();
                //baris ini -> statement untuk menyegarkan tampilan
                adapter.notifyDataSetChanged();
              }

              //baris ini -> statement untuk mendapatkan semua data dari koemntar yang ada dalam database
              KomentarService.getInstance(getActivity()).getAll(KomentarFragment.this, new Komentar());
            }
          })
          //baris ini -> statement untuk mendeteksi jika user memilih button Tidak
          .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
            }
          })
          //baris ini -> statement untuk menampilkan dialog
          .show();
    }

    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> method bawaan Adapter yang berfungsi untuk mendeteksi klik pada item
  @Override public void onItemClick(View view, int position) {
    //baris ini -> inisiasi variabel Komentar dengan nama 'komentar' untuk mendapatkan object komentar sesuai posisi
    final Komentar komentar = komentars.get(position);
    //baris ini -> statement untuk memulai activity baru (DetailActivity) dengan membawa data id komentar dari kelas ini
    startActivity(new Intent(getActivity(), DetailActivity.class).putExtra("id", komentar.getId()));
  }

  //baris ini -> method bawaan Adapter yang berfungsi untuk mendeteksi klik yang lama pada item
  @Override public void onLongItemClick(View view, int position) {

  }
}
