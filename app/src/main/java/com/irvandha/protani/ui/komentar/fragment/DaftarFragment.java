package com.irvandha.protani.ui.komentar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.KomentarService;
import com.irvandha.protani.ui.adapter.OneItemAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * kelas ini berfungsi sebagai tampilan list dari semua komentar
 */
public class DaftarFragment extends Fragment implements KomentarService.AllView {

  //baris ini -> deklarasi variabel String dengan nama 'ID_JENIS_KOMENTAR' yang berfungsi untuk sebagai penanda id jenis komentar
  private static final String ID_JENIS_KOMENTAR = "id_jenis_hama";
  //baris ini -> deklarasi variabel String dengan nama 'ID_TANAMAN' yang berfungsi untuk penanda id tanaman
  private static final String ID_TANAMAN = "id_tanaman";

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_announcer dengan nama variabel tvAnnouncer
  @BindView(R.id.tv_announcer) TextView tvAnnouncer;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi RecyclerView dengan id R.id.recyclerView dengan nama variabel recyclerView
  @BindView(R.id.recyclerView) RecyclerView recyclerView;
  //baris ini -> inisiasi variabel Unbinder dengan nama unbinder yang berfungsi untuk melepaskan pendeteksian view ketika kelas fragment ini selesai digunakan
  Unbinder unbinder;

  //baris ini -> deklarasi variabel int (Integer) dengan nama 'idJenisKomentar' yang berfungsi untuk menyimpan id jenis komentar pada kelas ini
  private int idJenisKomentar;
  //baris ini -> deklarasi variabel int (Integer) dengan nama 'idTanaman' yang berfungsi untuk menyimpan id tanaman pada kelas ini
  private int idTanaman;

  //baris ini -> deklarasi variabel array dengan nama 'komentars'
  private List<Komentar> komentars;
  //baris ini -> deklarasi variabel OneItemAdapter dengan nama 'adapter'
  private OneItemAdapter adapter;

  /**
   * method newInstance untuk membuat bentuk singleton dari kelas fragment ini
   *
   * @param idJenisKomentar untuk menerima id jenis di saat kelas ini di inisiasi
   * @param idTanaman untuk menerima id jenis di saat kelas ini di inisiasi
   * @return instance dari kelas ini
   */
  public static DaftarFragment newInstance(int idJenisKomentar, int idTanaman) {
    //baris ini -> inisiasi variabel sebagai instance dari kelas ini
    DaftarFragment fragment = new DaftarFragment();
    //baris ini -> inisiasi variabel Bundle dengan nama 'args' yang akan digunakan untuk melempar data ke dalam kelas ini
    Bundle args = new Bundle();
    //baris ini -> statement untuk menyimpan data dari parameter
    args.putInt(ID_JENIS_KOMENTAR, idJenisKomentar);
    //baris ini -> statement untuk menyimpan data dari parameter
    args.putInt(ID_TANAMAN, idTanaman);
    //baris ini -> statement untuk set variabel args sebagai arguments dari kelas ini
    fragment.setArguments(args);
    //baris ini -> statement untuk nilai kembalian dari method ini
    return fragment;
  }

  //baris ini -> method bawaan Fragment yang akan di eksekusi pertama kali
  @Override public void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk mengecek apakah arguments dari kelas ini tidak kosong
    if (getArguments() != null) {
      //baris ini -> statement untuk set nilai variabel idJenisKomentar dari arguments
      idJenisKomentar = getArguments().getInt(ID_JENIS_KOMENTAR);
      //baris ini -> statement untuk set nilai variabel idTanaman dari arguments
      idTanaman = getArguments().getInt(ID_TANAMAN);
    }
  }

  //baris ini -> method bawaan Fragment yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    //baris ini -> inisiasi variabel view dari layout dengan id R.id.fragment_daftar
    View view = inflater.inflate(R.layout.fragment_daftar, container, false);
    //baris ini -> inisiasi variabel unbinder sebagai instance dari kelas ButterKnife
    unbinder = ButterKnife.bind(this, view);
    //baris ini -> inisiasi variabel komentars
    komentars = new ArrayList<>();

    //baris ini -> inisiasi variabel adapter
    adapter = new OneItemAdapter();
    //baris ini -> memberikan nilai ke pada method setter komentars pada adapter tersebut
    adapter.setKomentars(komentars);

    //baris ini -> memberikan penugasan layout manager ke pada method setter layout manager pada recyclerView
    recyclerView.setLayoutManager(
        new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    //baris ini -> memberikan penugasan adapter ke pada method setter adapter pada recyclerView
    recyclerView.setAdapter(adapter);

    //baris ini -> inisiasi variabel Komentar dengan nama 'komentar'
    Komentar komentar = new Komentar();
    //baris ini -> statement untuk set object jenis ke variabel komentar
    komentar.setJenis(new Komentar.Jenis(idJenisKomentar));
    //baris ini -> statement untuk set object tanaman ke variabel komentar
    komentar.setTanaman(new Tanaman(idTanaman));

    //baris ini -> statement untuk mendapatkan semua data dari koemntar yang ada dalam database
    KomentarService.getInstance(getActivity()).getAll(this, komentar);

    //baris ini -> statement untuk nilai kembali dengan menggunakan variabel view
    return view;
  }

  //baris ini -> method bawaan Fragment yang dieksekusi jika fragment telah berhenti
  @Override public void onDestroyView() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onDestroyView();
    //baris ini -> statement untuk melepaskan semua view yang di identifikasi pafa fragment ini
    unbinder.unbind();
  }

  //baris ini -> method bawaan AllView dari KomentarService yang dieksekusi jika data gagal di ambil dari database
  @Override public void onSuccess(List<Komentar> komentars, String message) {
    //baris ini -> statement untuk mengecek apakah variabel komentars telah di inisiasi dan tidak kosong
    if (komentars != null && !komentars.isEmpty()) {
      //baris ini -> statement untuk menambahkan semua data dari database ke dalam variabel komentars
      this.komentars.addAll(komentars);
      //baris ini -> statement menyegarkan tampilan jika data berhasil dari database
      adapter.notifyDataSetChanged();
      //baris ini -> statement untuk menyembunyikan textview
      tvAnnouncer.setVisibility(View.GONE);
    } else {
      //baris ini -> statement memberikan nilai ke tvAnnouncer
      tvAnnouncer.setText("Belum ada data");
    }
  }

  //baris ini -> method bawaan AllView dari KomentarService yang dieksekusi jika data gagal di ambil dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement memberikan nilai ke tvAnnouncer
    tvAnnouncer.setText("Belum ada data");
  }
}
