package com.irvandha.protani.ui.komentar;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.JenisKomentarService;
import com.irvandha.protani.service.KomentarService;
import com.irvandha.protani.util.UserUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * kelas ini berfungsi sebagai tampilan tambah komentar atau update komentar
 */
public class FormActivity extends AppCompatActivity
    implements JenisKomentarService.AllJenisView, KomentarService.SaveView, KomentarService.GetView {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Toolbar dengan id R.id.toolbar dengan nama variabel toolbar
  @BindView(R.id.toolbar) Toolbar toolbar;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi EditText dengan id R.id.nama dengan nama variabel nama
  @BindView(R.id.nama) EditText nama;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Spinner dengan id R.id.spinner_jenis dengan nama variabel spinnerJenis
  @BindView(R.id.spinner_jenis) Spinner spinnerJenis;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi EditText dengan id R.id.gejala dengan nama variabel gejala
  @BindView(R.id.gejala) EditText gejala;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi EditText dengan id R.id.perawatan dengan nama variabel perawatan
  @BindView(R.id.perawatan) EditText perawatan;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi EditText dengan id R.id.nama_pengirim dengan nama variabel namaPengirim
  @BindView(R.id.nama_pengirim) EditText namaPengirim;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi EditText dengan id R.id.email dengan nama variabel email
  @BindView(R.id.email) EditText email;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi FloatingActionButton dengan id R.id.fab dengan nama variabel fab
  @BindView(R.id.fab) FloatingActionButton fab;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_spinner dengan nama variabel tvSpinner
  @BindView(R.id.tv_spinner) TextView tvSpinner;

  //baris ini -> deklarasi variabel array Jenis Komentar dengan nama 'jenisList'
  private List<Komentar.Jenis> jenisList;
  //baris ini -> deklarasi variabel Komentar dengan nama 'komentar'
  private Komentar komentar;

  //baris ini -> method bawaan AppCompatActivity yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override protected void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk mengatur tampilan dalam kelas ini dari layout R.layout.activity_form2
    setContentView(R.layout.activity_form2);
    //baris ini -> statement untuk memasang ButterKnife di kelas ini
    ButterKnife.bind(this);
    //baris ini -> statement untuk memasang toolbar pada kelas ini
    setSupportActionBar(toolbar);
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    setTitle("Tambah Komentar");
    //baris ini -> statement untuk inisiasi variabel ActionBar dengan nama 'actionBar'
    ActionBar actionBar = getSupportActionBar();
    //baris ini -> statement untuk mengecek apakah variabel actionBar telah di inisiasi dan set home display ditampilkan
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
    //baris ini -> statement untuk mengambil semua data komentar dari database menggunakan JenisKomentarService
    JenisKomentarService.getInstance(this).getAll(this);
    //baris ini -> statement untuk mengecek apakah ada data id dari activity atau fragment sebelumnya
    if (getIntent() != null && getIntent().getIntExtra("id", 0) != 0) {
      //baris ini -> statement untuk mengambil data komentar dari database berdasarkan id nya
      KomentarService.getInstance(this).get(this, new Komentar(getIntent().getIntExtra("id", 0)));
    }
  }

  //baris ini -> method onFabClickListener dengan anotasi @OnClick dari ButterKnife yang berfungsi untuk mendeteksi button simpan
  @OnClick(R.id.fab) public void onFabClickListener(View view) {
    //baris ini -> statement apakah variabel nama kosong
    if (nama.getText().toString().trim().length() == 0) {
      //baris ini -> statement untuk memberi error ke variabel nama
      nama.setError("Nama masih kosong");
      //baris ini -> statement apakah variabel gejala kosong
    } else if (gejala.getText().toString().trim().length() == 0) {
      //baris ini -> statement untuk memberi error ke variabel gejala
      gejala.setError("Gejala masih kosong");
      //baris ini -> statement apakah variabel perawatan kosong
    } else if (perawatan.getText().toString().trim().length() == 0) {
      //baris ini -> statement untuk memberi error ke variabel perawatan
      perawatan.setError("Perawatan masih kosong");
      //baris ini -> statement apakah variabel spinnerJenis masih memilih posisi ke 0
    } else if (spinnerJenis.getSelectedItemPosition() == 0) {
      //baris ini -> statement untuk memberi error ke variabel tvSpinner
      tvSpinner.setError("Pilih salah satu jenis hama disamping");
      //baris ini -> statement apakah variabel namaPengirim kosong
    } else if (namaPengirim.getText().toString().trim().length() == 0) {
      //baris ini -> statement untuk memberi error ke variabel namaPengirim
      namaPengirim.setError("Nama Pengirim masih kosong");
      //baris ini -> statement apakah variabel email kosong
    } else if (email.getText().toString().trim().length() == 0) {
      //baris ini -> statement untuk memberi error ke variabel email
      email.setError("Email Pengirim masih kosong");
    } else {
      //baris ini -> statement untuk mengecek apakah variabel object komentar null
      if (komentar == null) {
        //baris ini -> statement untuk inisiasi Komentar dengan nama 'komentar'
        komentar = new Komentar();
        //baris ini -> statement untuk set nama pengirim dari variabel komentar
        komentar.setNamaPengirim(namaPengirim.getText().toString());
        //baris ini -> statement untuk set email pengirim dari variabel komentar
        komentar.setEmailPengirim(email.getText().toString());
        //baris ini -> statement untuk set status dari variabel komentar
        komentar.setStatus(UserUtil.getInstance(this).isLogin());
        //baris ini -> statement untuk set nama dari variabel komentar
        komentar.setNama(nama.getText().toString());
        //baris ini -> statement untuk set gejala dari variabel komentar
        komentar.setGejala(gejala.getText().toString());
        //baris ini -> statement untuk set perawatan dari variabel komentar
        komentar.setPerawatan(perawatan.getText().toString());
        //baris ini -> statement untuk set object jenis dari variabel komentar
        komentar.setJenis(jenisList.get(spinnerJenis.getSelectedItemPosition() - 1));
        //baris ini -> statement untuk set object tanaman dari variabel komentar
        komentar.setTanaman(new Tanaman(getIntent().getIntExtra("id_tanaman", 0)));
      } else {
        //baris ini -> statement untuk set nama pengirim dari variabel komentar
        komentar.setNamaPengirim(namaPengirim.getText().toString());
        //baris ini -> statement untuk set email pengirim dari variabel komentar
        komentar.setEmailPengirim(email.getText().toString());
        //baris ini -> statement untuk set status dari variabel komentar
        komentar.setStatus(UserUtil.getInstance(this).isLogin());
        //baris ini -> statement untuk set nama dari variabel komentar
        komentar.setNama(nama.getText().toString());
        //baris ini -> statement untuk set gejala dari variabel komentar
        komentar.setGejala(gejala.getText().toString());
        //baris ini -> statement untuk set perawatan dari variabel komentar
        komentar.setPerawatan(perawatan.getText().toString());
        //baris ini -> statement untuk set object jenis dari variabel komentar
        komentar.setJenis(jenisList.get(spinnerJenis.getSelectedItemPosition() - 1));
        //komentar.setTanaman(new Tanaman(getIntent().getIntExtra("id_tanaman", 0)));
      }

      //baris ini -> statement untuk menyimpan atau memperbarui data komentar dari kelas KomentarService
      //method ini akan menyimpan data komentar baru jika id = 0
      //method ini akan memperbarui data komentar jika id > 0
      KomentarService.getInstance(this).saveOrUpdate(this, komentar);
      //baris ini -> statement untuk eksekusi method send dengan 1 parameter bernilai true
      send(true);
    }
  }

  //baris ini -> method bawaan AppCompatActivity yang berfungsi untuk menedeteksi menu yang dipilih oleh user
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id android.R.id.home lalu mengakhiri activity ini
    if (item.getItemId() == android.R.id.home) finish();
    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> method bawaan AllJenisView dari JenisKomentarService yang dieksekusi jika semua data berhasil di ambil dari database
  @Override public void onSuccess(List<Komentar.Jenis> jenis, String message) {
    //baris ini -> statement untuk set variabel array jenisList dengan data yang didapat dari database
    jenisList = jenis;
    //baris ini -> statement untuk inisiasi variabel array String dengan nama 'strings'
    List<String> strings = new ArrayList<>();
    //baris ini -> statement untuk menambah item ke dalam variabel strings
    strings.add("-");
    //baris ini -> statement untuk perulangan item dari hasil semua data yang didapat dari database
    for (Komentar.Jenis item : jenis) {
      //baris ini -> statement untuk menambah nama dari item ke dalam variabel strings
      strings.add(item.getNama());
    }

    //baris ini -> statement untuk set adapter ke variabel spinnerJenis untuk menampilkan item jenis komentar
    spinnerJenis.setAdapter(
        new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, strings));
    //baris ini -> statement untuk mengecek apakah variabel komentar telah di inisiasi lalu set posisi pada variabel spinnerJenis berdasarkan id jenis dari variabel komentar
    if (komentar != null) spinnerJenis.setSelection(komentar.getJenis().getId());
  }

  //baris ini -> method bawaan GetView dari KomentarService yang dieksekusi jika data berhasil di ambil dari database
  @Override public void onSuccess(Komentar komentar, String message) {
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    setTitle("Edit Komentar");
    //baris ini -> statement untuk set variabel komentar dengan data yang didapat dari database
    this.komentar = komentar;
    //baris ini -> statement untuk set nilai variabel nama dengan nama komentar
    nama.setText(komentar.getNama());
    //baris ini -> statement untuk set nilai variabel gejala dengan gejala komentar
    gejala.setText(komentar.getGejala());
    //baris ini -> statement untuk set nilai variabel perawatan dengan perawatan komentar
    perawatan.setText(komentar.getPerawatan());
    //baris ini -> statement untuk set nilai variabel namaPengirim dengan nama pengirim komentar
    namaPengirim.setText(komentar.getNamaPengirim());
    //baris ini -> statement untuk set nilai variabel email dengan email pengirim komentar
    email.setText(komentar.getEmailPengirim());
    //baris ini -> statement untuk mengecek apakah variabel spinnerJenis telah di inisiasi
    if (spinnerJenis.getAdapter() != null) {
      //baris ini -> statement untuk set posisi dari variabel spinnerJenis
      spinnerJenis.setSelection(komentar.getJenis().getId());
    }
  }

  //baris ini -> method bawaan SaveView dari KomentarService yang dieksekusi jika data gagal di simpan ke database
  @Override public void onSuccess(String message) {
    String finalMessage = UserUtil.getInstance(this).isLogin() ?
        message : "Komentar sukses ditambahkan, admin akan memverifikasi data anda terlebih dahulu";
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, finalMessage, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk mengakhiri activity ini
    finish();
  }

  //baris ini -> method bawaan SaveView, GetView dari KomentarService yang dieksekusi jika data gagal di simpan ke database atau di ambil dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk eksekusi method send dengan 1 parameter bernilai false
    send(false);
  }

  //baris ini -> inisiasi method dengan nama 'send' dengan parameter boolean dengan nama 'isSending'
  private void send(boolean isSending) {
    //baris ini -> statement untuk membuat variabel nama bisa di edit atau tidak tergantung parameter isSending
    nama.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel gejala bisa di edit atau tidak tergantung parameter isSending
    gejala.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel perawatan bisa di edit atau tidak tergantung parameter isSending
    perawatan.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel spinnerJenis bisa di pilih atau tidak tergantung parameter isSending
    spinnerJenis.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel namaPengirim bisa di edit atau tidak tergantung parameter isSending
    namaPengirim.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel email bisa di edit atau tidak tergantung parameter isSending
    email.setEnabled(!isSending);
    //baris ini -> statement untuk membuat variabel fab bisa di tekan atau tidak tergantung parameter isSending
    fab.setEnabled(!isSending);
  }
}
