package com.irvandha.protani.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import com.irvandha.protani.R;
import com.crashlytics.android.Crashlytics;
import com.irvandha.protani.ui.beranda.BerandaActivity;
import io.fabric.sdk.android.Fabric;

/**
 * kelas ini berfungsi sebagai tampilan splash atau tampilan awal
 */
public class MainActivity extends AppCompatActivity {

  //baris ini -> method bawaan AppCompatActivity yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override protected void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> inisiasi Fabric untuk mendeteksi segala error yang terjadi pada aplikasi ini
    Fabric.with(this, new Crashlytics());
    //baris ini -> statement untuk mengatur tampilan dalam kelas ini dari layout R.layout.activity_main
    setContentView(R.layout.activity_main);
    //baris ini -> statement Handler untuk memberikan waktu 3 detik pada activity
    new Handler(getMainLooper()).postDelayed(new Runnable() {
      @Override public void run() {
        //baris ini -> statement untuk memulai activity baru (BerandaActivity)
        startActivity(new Intent(MainActivity.this, BerandaActivity.class));
        //baris ini -> statement untuk mengakhiri activity ini
        finish();
      }
    }, 3000);
  }
}
