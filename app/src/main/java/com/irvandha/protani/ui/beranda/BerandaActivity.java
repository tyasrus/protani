package com.irvandha.protani.ui.beranda;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.irvandha.protani.R;
import com.irvandha.protani.ui.LoginActivity;
import com.irvandha.protani.ui.beranda.fragment.KomentarFragment;
import com.irvandha.protani.ui.beranda.fragment.TanamanFragment;
import com.irvandha.protani.ui.beranda.fragment.TentangFragment;
import com.irvandha.protani.util.UserUtil;

/**
 * kelas ini berfungsi sebagai tampilan utama dari aplikasi ini
 */
public class BerandaActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi Toolbar dengan id R.id.toolbar dengan nama variabel toolbar
  @BindView(R.id.toolbar) Toolbar toolbar;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi NavigationView dengan id R.id.nav_view dengan nama variabel navView
  @BindView(R.id.nav_view) NavigationView navView;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi DrawerLayout dengan id R.id.drawer_layout dengan nama variabel drawerLayout
  @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi RelativeLayout dengan id R.id.beranda_container dengan nama variabel berandaContainer
  @BindView(R.id.beranda_container) RelativeLayout berandaContainer;

  //baris ini -> deklarasi variabel BerandaCommunicator dari kelas ini dengan nama 'berandaCommunicator'
  public static BerandaCommunicator berandaCommunicator;
  //baris ini -> inisiasi variabel int (integer) dengan nama 'navId'
  private int navId = 0;

  //baris ini -> method bawaan AppCompatActivity yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override protected void onCreate(Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk mengatur tampilan dalam kelas ini dari layout R.layout.activity_beranda
    setContentView(R.layout.activity_beranda);
    //baris ini -> statement untuk memasang ButterKnife di kelas ini
    ButterKnife.bind(this);
    //baris ini -> statement untuk memasang toolbar pada kelas ini
    setSupportActionBar(toolbar);

    //baris ini -> inisiasi variabel ActionBarDrawerToggle dengan nama 'toggle' dengan beberapa parameter
    ActionBarDrawerToggle toggle =
        new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open,
            R.string.navigation_drawer_close);
    //baris ini -> statement untuk set variabel toggle sebagai listener ke dalam variabel drawerLayout
    drawerLayout.addDrawerListener(toggle);
    //baris ini -> statement untuk mengatur sinkronisasi variabel toggle berfungsi mendeteksi menu yang dipilih user
    toggle.syncState();

    //baris ini -> statement untuk set listener untuk variabel navView
    navView.setNavigationItemSelectedListener(this);

    //baris ini -> statement untuk menambahkan fragment TanamanFragment kedalam view dengan id dari variabel berandaContainer
    getSupportFragmentManager().beginTransaction().add(berandaContainer.getId(), new TanamanFragment(), TanamanFragment.class.getName()).commit();
  }

  //baris ini -> method bawaan AppCompatActivity yang dieksekusi ketika activity ini sudah tampil
  @Override protected void onStart() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onStart();
    //baris ini -> statement untuk set item yang ditandai
    navView.setCheckedItem(navId != 0 ? navId : R.id.nav_beranda);
    //baris ini -> statement untuk identifikasi item dengan id R.id.nav_komentar lalu akan tampil jika admin telah masuk, jika belum maka tidak tampil
    navView.getMenu().findItem(R.id.nav_komentar).setVisible(UserUtil.getInstance(this).isLogin());
    //baris ini -> statement untuk identifikasi item dengan id R.id.nav_logout lalu akan tampil jika admin telah masuk, jika belum maka tidak tampil
    navView.getMenu().findItem(R.id.nav_logout).setVisible(UserUtil.getInstance(this).isLogin());
    //baris ini -> statement untuk identifikasi item dengan id R.id.nav_login lalu akan tampil jika admin telah masuk, jika belum maka tidak tampil
    navView.getMenu().findItem(R.id.nav_login).setVisible(!UserUtil.getInstance(this).isLogin());
    //baris ini -> statement untuk mengecek apakah admin telah login dan menu dari variabel navView telah di inisiasi
    if (UserUtil.getInstance(this).isLogin() && navView.getMenu() != null) {
      //baris ini -> statement untuk mengecek variabel berandaCommunicator telah di inisiasi dan mengeksekusi method yang dipunyai variabel berandaCommunicator
      if (berandaCommunicator != null) berandaCommunicator.onBerandaStartListener();
    }
  }

  //baris ini -> method bawaan AppCompatActivity yang dieksekusi ketika button back dari handphone android ditekan
  @Override public void onBackPressed() {
    //baris ini -> statement untuk mengecek menu dari variabel drawerLayout masih terbuka
    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
      //baris ini -> statement untuk menutup menu dari variabel drawerLayout
      drawerLayout.closeDrawer(GravityCompat.START);
    } else {
      //baris ini -> implementasi method ini ke dalam super class
      super.onBackPressed();
    }
  }

  //baris ini -> method bawaan NavigationView yang dieksekusi ketika user memilih menu di menu navigation view
  @SuppressWarnings("StatementWithEmptyBody") @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    //baris ini -> inisiasi variabel int dengan nama 'id'
    int id = item.getItemId();

    //baris ini -> statement jika user memilih dengan menu dengan id R.id.nav_beranda
    if (id == R.id.nav_beranda) {
      //baris ini -> inisiasi variabel navId
      navId = id;
      //baris ini -> statement untuk mengganti fragment TanamanFragment kedalam view dengan id dari variabel berandaContainer
      getSupportFragmentManager().beginTransaction().replace(berandaContainer.getId(), new TanamanFragment()).addToBackStack(TanamanFragment.class.getName()).commit();
      //baris ini -> statement jika user memilih dengan menu dengan id R.id.nav_komentar
    } else if (id == R.id.nav_komentar) {
      //baris ini -> inisiasi variabel navId
      navId = id;
      //baris ini -> statement untuk mengganti fragment KomentarFragment kedalam view dengan id dari variabel berandaContainer
      getSupportFragmentManager().beginTransaction().replace(berandaContainer.getId(), new KomentarFragment()).addToBackStack(KomentarFragment.class.getName()).commit();
      //baris ini -> statement jika user memilih dengan menu dengan id R.id.nav_login
    } else if (id == R.id.nav_login) {
      //baris ini -> statement untuk memulai activity baru (LoginActivity)
      startActivity(new Intent(this, LoginActivity.class));
      //baris ini -> statement jika user memilih dengan menu dengan id R.id.nav_tentang
    } else if (id == R.id.nav_tentang) {
      //baris ini -> inisiasi variabel navId
      navId = id;
      //baris ini -> statement untuk mengganti fragment TentangFragment kedalam view dengan id dari variabel berandaContainer
      getSupportFragmentManager().beginTransaction().replace(berandaContainer.getId(), new TentangFragment()).addToBackStack(TentangFragment.class.getName()).commit();
      //baris ini -> statement jika user memilih dengan menu dengan id R.id.nav_logout
    } else if (id == R.id.nav_logout) {
      //baris ini -> statement untuk menghapus session dengan logout dari aplikasi ini
      UserUtil.getInstance(this).logout();
      //baris ini -> statement untuk memulai activity baru (BerandaActivity)
      startActivity(new Intent(this, BerandaActivity.class));
      //baris ini -> statement untuk mengakhiri activity ini
      finish();
    }

    //baris ini -> statement untuk menutup menu dari variabel drawerLayout
    drawerLayout.closeDrawer(GravityCompat.START);
    //baris ini -> statement untuk nilai kembali default true
    return true;
  }

  //baris ini -> kelas interface listener untuk kelas ini
  public interface BerandaCommunicator {
    void onBerandaStartListener();
  }
}
