package com.irvandha.protani.ui.adapter;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.util.UserUtil;
import java.util.List;

/**
 * kelas ini berfungsi sebagai perantara tampilan listing di seluruh aplikasi ini
 */
public class OneItemAdapter extends RecyclerView.Adapter<OneItemAdapter.ViewHolder> {

  //baris ini -> deklarasi variabel array dengan nama tanamans untuk menampung data object tanaman
  private List<Tanaman> tanamans;
  //baris ini -> deklarasi variabel array dengan nama komentars untuk menampung data object komentar
  private List<Komentar> komentars;
  //baris ini -> deklarasi variabel array dengan nama komentars untuk menampung data object komentar
  private List<Komentar.Jenis> jenisKomentars;
  //baris ini -> deklarasi variabel boolean dengan nama isAdmin untuk mendeteksi jika admin telah login atau belum
  private boolean isAdmin;
  //baris ini -> deklarasi variabel boolean dengan nama isOnlyTitle untuk mendeteksi jika ingin menampilkan hanya judul dari item
  private boolean isOnlyTitle;
  //baris ini -> setter listener untuk kelas yang menggunakan view pada kelas ini
  private OnItemClickListener onItemClickListener;

  //baris ini -> constructor yang berfungsi sebagai instance kelas atau perwakilan kelas untuk digunakan oleh kelas lain
  public OneItemAdapter() {
  }

  //baris ini -> constructor yang berfungsi sebagai instance kelas atau perwakilan kelas untuk digunakan oleh kelas lain
  //untuk constructor ini menggunakan parameter boolean dengan nama isAdmin, untuk memberikan nilai ke variabel isAdmin
  public OneItemAdapter(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public void setOnlyTitle(boolean onlyTitle) {
    isOnlyTitle = onlyTitle;
  }

  //baris ini -> setter untuk variabel tanamans
  public void setTanamans(List<Tanaman> tanamans) {
    this.tanamans = tanamans;
  }

  //baris ini -> setter untuk variabel komentars
  public void setKomentars(List<Komentar> komentars) {
    this.komentars = komentars;
  }

  //baris ini -> setter untuk variabel jenis komentar
  public void setJenisKomentars(List<Komentar.Jenis> jenisKomentars) {
    this.jenisKomentars = jenisKomentars;
  }

  //baris ini -> setter listener untuk kelas yang menggunakan view pada kelas ini
  public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
    this.onItemClickListener = onItemClickListener;
  }

  //baris ini -> method bawaan kelas RecyclerView.Adapter untuk inisiasi layout yang berfungsi untuk tampilan item list
  @Override public OneItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    //baris ini -> inisiasi variabel view dari layout dengan id R.id.one_item_layout
    View view =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.one_item_layout, parent, false);
    //baris ini -> nilai kembalian method ini menggunakan holder yang telah di inisiasi
    return new ViewHolder(view);
  }

  //baris ini -> method bawaan kelas RecyclerView.Adapter untuk mengisi data ke dalam tampilan item list
  @Override public void onBindViewHolder(OneItemAdapter.ViewHolder holder, int position) {
    //baris ini -> deklarasi variabel html untuk isian texview
    String html = "";
    //baris ini -> untuk mengecek jika variabel tanamans ada isinya
    if (tanamans != null) {
      //baris ini -> inisiasi variabel Tanaman dengan nama 'tanaman' untuk mendapatkan object tanaman sesuai posisi
      Tanaman tanaman = tanamans.get(position);
      //baris ini -> inisiasi variabel string dengan nama 'status' dengan isian tergantung dari apakah tanaman sudah di verifikasi atau belum dan apakah admin telah login atau belum
      //berfungsi untuk memberi tahu user jika tanaman sudah diverifikasi atau belum
      String status =
          tanaman.isStatus() && UserUtil.getInstance(holder.itemView.getContext()).isLogin()
              ? "Sudah terverifikasi" : "Belum Terverifikasi";
      //baris ini -> inisiasi variabel string dengan nama 'color' dengan isian tergantung dari apakah tanaman sudah di verifikasi atau belum dan apakah admin telah login atau belum
      //berfungsi untuk memberikan warna pada tulisan jika tanaman sudah di verifikasi atau belum
      String color =
          tanaman.isStatus() && UserUtil.getInstance(holder.itemView.getContext()).isLogin()
              ? "#1976D2" : "#d32f2f";
      //baris ini -> inisiasi variabel string dengan nama 'html' dengan isian dari gabungan nama dan status
      html = UserUtil.getInstance(holder.itemView.getContext()).isLogin() ? "<b>"
          + tanaman.getNama()
          + " </b>"
          + "<br/>"
          + "Status : <font color=\""
          + color
          + "\">"
          + status
          + "</font>" : "<b>" + tanaman.getNama() + " </b>";
    }

    //baris ini -> untuk mengecek jika variabel komentars ada isinya
    if (komentars != null) {
      //baris ini -> inisiasi variabel Komentar dengan nama 'komentar' untuk mendapatkan object komentar sesuai posisi
      Komentar komentar = komentars.get(position);
      //baris ini -> inisiasi variabel string dengan nama 'status' dengan isian tergantung dari apakah komentar sudah di verifikasi atau belum
      //berfungsi untuk memberi tahu user jika komentar sudah diverifikasi atau belum
      String status = komentar.isStatus() ? "Sudah terverifikasi" : "Belum Terverifikasi";
      //baris ini -> inisiasi variabel string dengan nama 'color' dengan isian tergantung dari apakah komentar sudah di verifikasi atau belum
      //berfungsi untuk memberikan warna pada tulisan jika komentar sudah di verifikasi atau belum
      String color = komentar.isStatus() ? "#1976D2" : "#d32f2f";
      //baris ini -> inisiasi variabel string dengan nama 'html' dengan isian dari gabungan semua field yang ada pada komentar
      html = isAdmin ? "<b>Dari : </b><br/>"
          + komentar.getNamaPengirim()
          + " ("
          + komentar.getEmailPengirim()
          + ")"
          + "<br/><br/>"
          + "<b>Status : </b><br/>"
          + "<b><font color=\""
          + color
          + "\">"
          + status
          + "</font></b>" : "<b>Nama : </b><br/>"
          + komentar.getNama()
          + "<br/><br/>"
          + "<b>Gejala : </b><br/>"
          + komentar.getGejala()
          + "<br/><br/>"
          + "<b>Perawatan : </b><br/>"
          + komentar.getPerawatan()
          + "<br/><br/>"
          + "<b>Dari : </b><br/>"
          + komentar.getNamaPengirim()
          + " ("
          + komentar.getEmailPengirim()
          + ")";

      html = isOnlyTitle ? "<b>" + komentar.getNama() + "</b>" : html;
    }

    if (jenisKomentars != null) {
      Komentar.Jenis jenis = jenisKomentars.get(position);
      html = "<b>" + jenis.getNama() + "</b>";
    }

    //baris ini -> statement untuk menampilkan isian kedalam textview
    holder.tvFirst.setText(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? Html.fromHtml(html,
        Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(html));
  }

  //baris ini -> method ini berfungsi untuk memberikan jumlah item yang akan di load dalam list
  @Override public int getItemCount() {
    return tanamans != null ? tanamans.size()
        : komentars != null ? komentars.size() : jenisKomentars != null ? jenisKomentars.size() : 0;
  }

  //baris ini -> kelas interface listener untuk kelas ini
  public interface OnItemClickListener {
    void onItemClick(View view, int position);

    void onLongItemClick(View view, int position);
  }

  /**
   * kelas ini berfungsi sebagai pengelola view dari file xml di tampilan list
   */
  class ViewHolder extends RecyclerView.ViewHolder
      implements View.OnClickListener, View.OnLongClickListener {

    //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
    //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_first dengan nama variabel tvFirst
    @BindView(R.id.tv_first) TextView tvFirst;

    //baris ini -> constructor sebagai instance untuk inisiasi kelas ini
    ViewHolder(View itemView) {
      //baris ini -> implementasi method ini ke dalam super class
      super(itemView);
      //baris ini -> statement untuk memasang ButterKnife di kelas ini
      ButterKnife.bind(this, itemView);
      //baris ini -> statement untuk memasang deteksi klik pada view di kelas ini
      itemView.setOnClickListener(this);
      //baris ini -> statement untuk memasang deteksi klik yang lama pada view di kelas ini
      itemView.setOnLongClickListener(this);
    }

    //baris ini -> method onCLick berfungsi untuk mendeteksi klik user di view ini
    @Override public void onClick(View view) {
      //baris ini -> statement untuk pengecekan apakah listener dari kelas adapter telah di inisiasi, jika sudah ter inisiasi maka method onItemClick di eksekusi
      if (onItemClickListener != null) onItemClickListener.onItemClick(view, getAdapterPosition());
    }

    //baris ini -> method onCLick berfungsi untuk mendeteksi klik yang lama user di view ini
    @Override public boolean onLongClick(View view) {
      //baris ini -> statement untuk pengecekan apakah listener dari kelas adapter telah di inisiasi, jika sudah ter inisiasi maka method onLongItemClick di eksekusi
      if (onItemClickListener != null) {
        onItemClickListener.onLongItemClick(view, getAdapterPosition());
      }
      //baris ini -> statement untuk nilai kembalian dari method ini
      return false;
    }
  }
}
