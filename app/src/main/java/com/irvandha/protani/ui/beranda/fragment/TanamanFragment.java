package com.irvandha.protani.ui.beranda.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.irvandha.protani.R;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.TanamanService;
import com.irvandha.protani.ui.beranda.BerandaActivity;
import com.irvandha.protani.ui.adapter.OneItemAdapter;
import com.irvandha.protani.ui.tanaman.DetailActivity;
import com.irvandha.protani.ui.tanaman.FormActivity;
import com.irvandha.protani.util.UserUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * kelas ini berfungsi sebagai tampilan tanamans semua tanaman serta berfungsi sebagai tampilan utama dari aplikasi ini
 */
public class TanamanFragment extends Fragment
    implements OneItemAdapter.OnItemClickListener, TanamanService.AllView,
    BerandaActivity.BerandaCommunicator, TanamanService.DeleteView {

  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi RecyclerView dengan id R.id.recyclerView dengan nama variabel recyclerView
  @BindView(R.id.recyclerView) RecyclerView recyclerView;
  //@BindView adalah annotasi dari library ButterKnife untuk mengidentifikasi view pada xml
  //pada kelas ini digunakan untuk mengidentifikasi TextView dengan id R.id.tv_announcer dengan nama variabel tvAnnouncer
  @BindView(R.id.tv_announcer) TextView tvAnnouncer;
  //baris ini -> inisiasi variabel Unbinder dengan nama unbinder yang berfungsi untuk melepaskan pendeteksian view ketika kelas fragment ini selesai digunakan
  Unbinder unbinder;

  //baris ini -> deklarasi variabel array dengan nama tanamans
  private List<Tanaman> tanamans;
  //baris ini -> deklarasi variabel OneItemAdapter dengan nama adapter
  private OneItemAdapter adapter;
  //baris ini -> deklarasi variabel Menu dengan nama menu
  private Menu menu;

  //baris ini -> method bawaan Fragment yang akan di eksekusi pertama kali
  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreate(savedInstanceState);
    //baris ini -> statement untuk menandakan bahwa kelas ini akan menggunakan menu di toolbar (tampilan kotak diatas)
    setHasOptionsMenu(true);
  }

  //baris ini -> method bawaan Fragment yang digunakan untuk mengimplementasi tampilan pada kelas ini
  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    //baris ini -> inisiasi variabel view dari layout dengan id R.id.fragment_tanaman
    View view = inflater.inflate(R.layout.fragment_tanaman, container, false);
    //baris ini -> inisiasi variabel unbinder sebagai instance dari kelas ButterKnife
    unbinder = ButterKnife.bind(this, view);
    //baris ini -> inisiasi variabel tanamans
    tanamans = new ArrayList<>();

    //baris ini -> inisiasi variabel adapter
    adapter = new OneItemAdapter();
    //baris ini -> memberikan nilai ke pada method setter tanamans pada adapter tersebut
    adapter.setTanamans(tanamans);
    //baris ini -> memberikan nilai ke pada method setter listener pada adapter tersebut
    adapter.setOnItemClickListener(this);

    //baris ini -> memberikan penugasan layout manager ke pada method setter layout manager pada recyclerView
    recyclerView.setLayoutManager(
        new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    //baris ini -> memberikan penugasan adapter ke pada method setter adapter pada recyclerView
    recyclerView.setAdapter(adapter);
    //baris ini -> memberikan penugasan variabel berandaCommunicator dari kelas BerandaActivity kedalam kelas ini
    BerandaActivity.berandaCommunicator = this;

    //baris ini -> statement untuk mendapatkan semua data dari tanaman yang ada dalam database
    TanamanService.getInstance(getActivity()).getAll(this);

    //baris ini -> statement untuk nilai kembali dengan menggunakan variabel view
    return view;
  }

  //baris ini -> method bawaan Fragment yang dieksekusi jika fragment ini digunakan lagi
  @Override public void onResume() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onResume();
    //baris ini -> statement untuk mengubah judul pada toolbar (tampilan kotak diatas)
    getActivity().setTitle("Daftar Tanaman");
  }

  //baris ini -> method bawaan Fragment yang berfungsi untuk inisiasi menu di toolbar (tampilan kotak diatas)
  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //baris ini -> implementasi method ini ke dalam super class
    super.onCreateOptionsMenu(menu, inflater);
    //baris ini -> inisiasi menu dengan menggunakan menu dengan id R.menu.beranda
    inflater.inflate(R.menu.beranda, menu);
    //baris ini -> statement untuk memberikan nilai ke dalam variabel menu
    this.menu = menu;
    //baris ini -> statement untuk menyembunyikan menu item dengan id R.id.action_edit
    menu.findItem(R.id.action_edit).setVisible(false);
  }

  //baris ini -> method bawaan Fragment yang berfungsi untuk menedeteksi menu yang dipilih oleh user
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    //baris ini -> statement jika user memilih menu item dengan id R.id.action_tambah
    if (item.getItemId() == R.id.action_tambah) {
      //baris ini -> statement untuk memulai activity baru (FormActivity)
      startActivity(new Intent(getActivity(), FormActivity.class));
    }

    //baris ini -> statement jika user memilih menu item dengan id R.id.action_refresh
    if (item.getItemId() == R.id.action_refresh) {
      //baris ini -> statement untuk menampilkan dialog
      new AlertDialog.Builder(getActivity())
          //baris ini -> statement untuk memberikan isi konten dialog
          .setMessage("Anda akan merefresh halaman ini, lanjutkan?")
          //baris ini -> statement untuk mendeteksi jika user memilih button Ya
          .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
              //baris ini -> statement untuk mengecek jika variabel tanamans tidak kosong
              if (!tanamans.isEmpty()) {
                //baris ini -> statement untuk menghapus semua item dalam variabel tanamans
                tanamans.clear();
                //baris ini -> statement untuk menyegarkan tampilan
                adapter.notifyDataSetChanged();
              }

              //baris ini -> statement untuk mendapatkan semua data dari tanaman yang ada dalam database
              TanamanService.getInstance(getActivity()).getAll(TanamanFragment.this);
            }
          })
          //baris ini -> statement untuk mendeteksi jika user memilih button Tidak
          .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk menghilangkan dialog
              dialogInterface.dismiss();
            }
          })
          .show();
    }

    //baris ini -> statement untuk nilai kembalian secara default dari method ini
    return super.onOptionsItemSelected(item);
  }

  //baris ini -> method bawaan Fragment yang dieksekusi jika fragment telah berhenti
  @Override public void onDestroyView() {
    //baris ini -> implementasi method ini ke dalam super class
    super.onDestroyView();
    //baris ini -> statement untuk melepaskan semua view yang di identifikasi pafa fragment ini
    unbinder.unbind();
  }

  //baris ini -> method bawaan Adapter yang berfungsi untuk mendeteksi klik pada item
  @Override public void onItemClick(View view, int position) {
    //baris ini -> statement untuk memulai activity baru (DetailActivity) dengan membawa data id tanaman dari kelas ini
    startActivity(
        new Intent(getActivity(), DetailActivity.class).putExtra("id", tanamans.get(position).getId()));
  }

  //baris ini -> method bawaan Adapter yang berfungsi untuk mendeteksi klik yang lama pada item
  @Override public void onLongItemClick(View view, int position) {
    //baris ini -> inisiasi variabel Tanaman dengan nama 'tanaman' untuk mendapatkan object tanaman sesuai posisi
    final Tanaman tanaman = tanamans.get(position);
    //baris ini -> statement jika admin telah melakukan login
    if (UserUtil.getInstance(getActivity()).isLogin()) {
      //baris ini -> statement untuk menampilkan dialog
      new AlertDialog.Builder(getActivity())
          //baris ini -> statement untuk memberikan judul dialog
          .setTitle("Options")
          //baris ini -> statement untuk menampilkan pilihan edit dan hapus
          .setItems(new String[] { "Edit", "Hapus" }, new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              //baris ini -> statement untuk mendeteksi jika i == 0 yang berarti user memilih edit
              if (i == 0) {
                //baris ini -> statement untuk memulai activity baru (FormActivity) dengan membawa data id tanaman dari kelas ini
                startActivity(
                    new Intent(getActivity(), FormActivity.class).putExtra("id", tanaman.getId()));
              }

              //baris ini -> statement untuk mendeteksi jika i == 1 yang berarti user memilih hapus
              if (i == 1) {
                //baris ini -> statement untuk menghapus tanaman yang telah dipilih user
                TanamanService.getInstance(getActivity()).delete(TanamanFragment.this, tanaman);
              }
            }
          })
          //baris ini -> statement untuk menampilkan dialog
          .show();
    }
  }

  //baris ini -> method bawaan AllView dari TanamanService yang dieksekusi jika data berhasil di ambil dari database
  @Override public void onSuccess(List<Tanaman> tanamans, String message) {
    //baris ini -> statement untuk mengecek jika parameter tanamans ada dan tidak kosong
    if (tanamans != null && !tanamans.isEmpty()) {
      //baris ini -> statement untuk mengecek jika variabel tvAnnouncer ada lalu disembunyikan atau tidak ditampilkan
      if (tvAnnouncer != null) tvAnnouncer.setVisibility(View.GONE);

      //baris ini -> statement untuk menambahkan semua data dari database ke dalam variabel tanamans
      this.tanamans.addAll(tanamans);
      //baris ini -> statement menyegarkan tampilan jika data berhasil dari database
      adapter.notifyDataSetChanged();
    } else {
      //baris ini -> statement untuk mengecek jika variabel tvAnnouncer ada lalu menampilkan teks seperti dibawah
      if (tvAnnouncer != null) tvAnnouncer.setText("Data kosong, silahkan refresh kembali");
    }
  }

  //baris ini -> method bawaan DeleteView dari TanamanService yang dieksekusi jika data berhasil di hapus dari database
  @Override public void onSuccess(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk mengecek jika variabel tanamans tidak kosong
    if (!tanamans.isEmpty()) {
      //baris ini -> statement untuk menghapus semua item dalam variabel tanamans
      tanamans.clear();
      //baris ini -> statement menyegarkan tampilan jika data berhasil dari database
      adapter.notifyDataSetChanged();
    }

    //baris ini -> statement untuk mendapatkan semua data dari tanaman yang ada dalam database
    TanamanService.getInstance(getActivity()).getAll(TanamanFragment.this);
  }

  //baris ini -> method bawaan AllView, DeleteView dari TanamanService yang dieksekusi jika data gagal di ambil dari database
  @Override public void onFailed(String message) {
    //baris ini -> statement untuk menampilkan toast (peringatan)
    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    //baris ini -> statement untuk mengecek jika variabel tvAnnouncer ada lalu menampilkan teks seperti dibawah
    if (tvAnnouncer != null) tvAnnouncer.setText("Data kosong, silahkan refresh kembali");;
  }

  //baris ini -> method bawaan listener dari BerandaActivity
  @Override public void onBerandaStartListener() {
    //baris ini -> statement untuk mengecek jika variabel menu telah di inisiasi dan menu item dengan id R.id.action_tambah telah di inisiasi
    if (menu != null && menu.findItem(R.id.action_tambah) != null) {
      //baris ini -> statement untuk mengecek jika variabel tanamans tidak kosong
      if (!tanamans.isEmpty()) {
        //baris ini -> statement untuk menghapus semua item dalam variabel tanamans
        tanamans.clear();
        //baris ini -> statement menyegarkan tampilan jika data berhasil dari database
        adapter.notifyDataSetChanged();
      }

      //baris ini -> statement untuk mendapatkan semua data dari tanaman yang ada dalam database
      TanamanService.getInstance(getActivity()).getAll(TanamanFragment.this);
    }
  }
}
