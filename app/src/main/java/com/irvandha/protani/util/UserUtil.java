package com.irvandha.protani.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.irvandha.protani.model.User;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * kelas ini berfungsi untuk membuat session pada aplikasi ini
 */
public class UserUtil {

    //baris ini -> deklarasi variabel String dengan nama 'id_user' yang berfungsi untuk sebagai penanda id user
    private static final String ID_USER = "id_user";

    //baris ini -> deklarasi variabel SharedPreferences dengan nama 'sharedPreferences'
    private static SharedPreferences sharedPreferences;

    /**
     * method getInstance untuk membuat bentuk singleton dari kelas ini
     *
     * @param context untuk menerima context di saat kelas ini di inisiasi
     * @return instance dari kelas ini
     */
    public static UserUtil getInstance(Context context) {
        //baris ini -> inisiasi variabel sharedPreferences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        //baris ini -> statement untuk nilai kembalian dari method ini
        return new UserUtil();
    }

    //baris ini -> method setLoginState untuk mengatur status user saat login
    public void setLoginState(User user) {
        //baris ini -> statement untuk set nilai data sesuai nilai dari id parameter user dengan kunci variabel ID_USER
        sharedPreferences.edit().putInt(ID_USER, user.getId()).apply();
    }

    //baris ini -> method getId untuk mendapatkan id user dari penyimpanan sharedPreferences
    public int getId() {
        //baris ini -> statement untuk nilai kembalian dari method ini
        return sharedPreferences.getInt(ID_USER, 0);
    }

    //baris ini -> method isLogin untuk mendapatkan status login dari penyimpanan sharedPreferences
    public boolean isLogin() {
        //baris ini -> statement untuk nilai kembalian dari method ini
        return getId() != 0;
    }

    //baris ini -> method logout untuk menghapus semua session dalam penyimpanan sharedPreferences
    public void logout() {
        //baris ini -> statement untuk set nilai data menjadi 0 dengan kunci variabel ID_USER
        sharedPreferences.edit().putInt(ID_USER, 0).apply();
    }
}
