package com.irvandha.protani.service;

import android.app.Activity;
import android.os.NetworkOnMainThreadException;
import android.support.annotation.NonNull;
import com.irvandha.protani.BuildConfig;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.config.Api;
import com.irvandha.protani.service.config.BaseView;
import com.irvandha.protani.service.config.Message;
import com.irvandha.protani.util.UserUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * kelas ini digunakan untuk manipulasi data dari atau ke server khusus untuk tabel tanaman
 *
 * referensi : https://github.com/square/okhttp/wiki/Recipes
 * mohon dibaca link nya om
 */
public class TanamanService {

  private OkHttpClient client;
  private RequestBody body;
  private Request request;
  private Activity activity;

  /**
   * method getInstance untuk membuat bentuk singleton dari kelas ini
   *
   * @param activity parameter untuk melempar context activity ke dalam kelas ini
   * @return
   */
  public static TanamanService getInstance(Activity activity) {
    TanamanService instance = new TanamanService();
    if (instance.client == null) {
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }
    instance.activity = activity;
    return instance;
  }

  //method saveOrUpdate untuk menyimpan atau memperbarui data tanaman
  //method ini akan berfungsi menyimpan data tanaman jika id dari data tanaman yang dikirim belum ada atau bernilai 0
  //method ini akan berfungsi memperbarui data tanaman jika id dari data tanaman yang dikirim sudah ada atau bernilai lebih dari 0
  public void saveOrUpdate(final SaveView view, final Tanaman param) {
    body = new FormBody.Builder()
        .add("nama", String.valueOf(param.getNama()))
        .add("deskripsi", String.valueOf(param.getDeskripsi()))
        .add("status", String.valueOf(param.isStatus()))
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(BuildConfig.BASE_URL + Api.TANAMAN_SAVE)
        //http request post
        .post(body).build();

    if (param.getId() != 0) {
      body = new FormBody.Builder().add("id", String.valueOf(param.getId()))
          .add("nama", String.valueOf(param.getNama()))
          .add("deskripsi", String.valueOf(param.getDeskripsi()))
          .add("status", String.valueOf(param.isStatus()))
          .build();

      //request ini untuk menjalankan servis http ke server
      request = new Request.Builder().url(BuildConfig.BASE_URL + Api.TANAMAN_UPDATE)
          //http request post
          .post(body).build();
    }

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(@NonNull Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(@NonNull Call call, @NonNull final Response response)
          throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(jsonObject.getString("status"));
                } else {
                  view.onFailed(param.getId() != 0 ? Message.UPDATE_RESULT_FAILED
                      : Message.SAVE_RESULT_FAILED);
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(param.getId() != 0 ? Message.UPDATE_RESULT_NOT_FOUND
                    : Message.SAVE_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }//method saveOrUpdate untuk menyimpan atau memperbarui data tanaman
  //method ini akan berfungsi menyimpan data tanaman jika id dari data tanaman yang dikirim belum ada atau bernilai 0
  //method ini akan berfungsi memperbarui data tanaman jika id dari data tanaman yang dikirim sudah ada atau bernilai lebih dari 0
  public void saveWithKomentar(final SaveView view, final Komentar param) {
    body = new FormBody.Builder()
        .add("nama", String.valueOf(param.getTanaman().getNama()))
        .add("deskripsi", String.valueOf(param.getTanaman().getDeskripsi()))
        .add("status", String.valueOf(param.getTanaman().isStatus()))
        .build();

    if (param.getNama().trim().length() > 0) {
      body = new FormBody.Builder()
          .add("nama", String.valueOf(param.getTanaman().getNama()))
          .add("deskripsi", String.valueOf(param.getTanaman().getDeskripsi()))
          .add("status", String.valueOf(param.getTanaman().isStatus()))
          .add("nama_komentar", String.valueOf(param.getNama()))
          .add("id_jenis_komentar", String.valueOf(param.getJenis().getId()))
          .add("gejala_komentar", String.valueOf(param.getGejala()))
          .add("perawatan_komentar", String.valueOf(param.getPerawatan()))
          .add("nama_pengirim", String.valueOf(param.getNamaPengirim()))
          .add("email_pengirim", String.valueOf(param.getEmailPengirim()))
          .add("status_komentar", String.valueOf(param.isStatus()))
          .build();
    }

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(BuildConfig.BASE_URL + Api.TANAMAN_SAVE)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(@NonNull Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(@NonNull Call call, @NonNull final Response response)
          throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(jsonObject.getString("status"));
                } else {
                  view.onFailed(param.getId() != 0 ? Message.UPDATE_RESULT_FAILED
                      : Message.SAVE_RESULT_FAILED);
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(param.getId() != 0 ? Message.UPDATE_RESULT_NOT_FOUND
                    : Message.SAVE_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //method getAll untuk mengambil semua data dari tabel tanaman
  public void getAll(final AllView view) {
    String query = BuildConfig.BASE_URL + Api.TANAMAN_ALL;

    if (UserUtil.getInstance(activity).isLogin()) {
      query = query + "?is_login=true";
    }

    request = new Request.Builder().url(query).get().build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(@NonNull Call call, @NonNull final Response response)
          throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  List<Tanaman> results = new ArrayList<>();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonObject.getInt("rows") > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                      JSONObject dataObject = jsonArray.getJSONObject(i);
                      Tanaman result = new Tanaman();
                      result.setId(dataObject.getInt("id"));
                      result.setNama(dataObject.getString("nama"));
                      result.setDeskripsi(dataObject.getString("deskripsi"));
                      result.setStatus(dataObject.getBoolean("status"));
                      results.add(result);
                    }
                  }

                  view.onSuccess(results,
                      jsonObject.getInt("rows") > 0 ? Message.LIST_RESULT_SUCCESS
                          : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.LIST_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //method get untuk mengambil data dari tabel tanaman berdasarkan id
  public void get(final GetView view, Tanaman param) {
    request =
        new Request.Builder().url(BuildConfig.BASE_URL + Api.TANAMAN_GET + "?id=" + param.getId())
            .get()
            .build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(@NonNull Call call, @NonNull final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(@NonNull Call call, @NonNull final Response response)
          throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  Tanaman result = null;
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonArray.length() > 0) {
                    if (jsonArray.get(0) != null) {
                      result = new Tanaman();
                      JSONObject dataObject = jsonArray.getJSONObject(0);
                      result.setId(dataObject.getInt("id"));
                      result.setNama(dataObject.getString("nama"));
                      result.setDeskripsi(dataObject.getString("deskripsi"));
                      result.setStatus(dataObject.getBoolean("status"));
                    }
                  }

                  view.onSuccess(result,
                      result != null ? Message.LIST_RESULT_SUCCESS : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.LIST_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //method delete untuk menghapus data dari tabel tanaman berdasarkan id
  public void delete(final DeleteView view, Tanaman param) {
    body = new FormBody.Builder().add("id", String.valueOf(param.getId())).build();

    request =
        new Request.Builder().url(BuildConfig.BASE_URL + Api.TANAMAN_DELETE).post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(Message.DELETE_RESULT_SUCCESS);
                } else {
                  view.onFailed(Message.DELETE_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.DELETE_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //interface DeleteView untuk memberikan tanda ketika proses menghapus data terjadi ke dalam activity atau fragment
  public interface DeleteView {
    void onSuccess(String message);

    void onFailed(String message);
  }

  //interface SaveView untuk memberikan tanda ketika proses penyimpanan data terjadi ke dalam activity atau fragment
  public interface SaveView {
    void onSuccess(String message);

    void onFailed(String message);
  }

  //interface AllView untuk memberikan tanda ketika proses pengambilan semua data terjadi ke dalam activity atau fragment
  public interface AllView extends BaseView<List<Tanaman>> {
  }

  //interface GetView untuk memberikan tanda ketika proses pengambilan data terjadi ke dalam activity atau fragment
  public interface GetView {
    void onSuccess(Tanaman result, String message);

    void onFailed(String message);
  }
}
