package com.irvandha.protani.service;

import android.app.Activity;
import android.os.NetworkOnMainThreadException;
import com.irvandha.protani.BuildConfig;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.service.config.Api;
import com.irvandha.protani.service.config.BaseView;
import com.irvandha.protani.service.config.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * kelas ini digunakan untuk manipulasi data dari atau ke server khusus untuk tabel jenis_komentar
 */
public class JenisKomentarService {

  //baris ini -> deklarasi variabel OkHttpClient dengan nama 'client'
  private OkHttpClient client;
  //baris ini -> deklarasi variabel Activity dengan nama 'activity'
  private Activity activity;

  /**
   * method getInstance untuk membuat bentuk singleton dari kelas ini
   *
   * @param activity parameter untuk melempar context activity ke dalam kelas ini
   * @return
   */
  public static JenisKomentarService getInstance(Activity activity) {
    //baris ini -> inisiasi variabel JenisKomentarService sebagai instance dari kelas ini
    JenisKomentarService instance = new JenisKomentarService();
    //baris ini -> statement untuk mengecek apakah variabel client belum di inisiasi
    if (instance.client == null) {
      //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //baris ini -> statement untuk set level dari log ini
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      //baris ini -> statement untuk inisiasi variabel client dengan interceptor
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    //baris ini -> statement untuk set variabel activity
    instance.activity = activity;

    //baris ini -> statement untuk nilai kembalian dari method inis
    return instance;
  }

  //method getAll untuk mengambil semua data dari tabel jenis_komentar
  public void getAll(final AllJenisView view) {
    //baris ini -> inisiasi variabel request untuk digunakan sebagai penghubung data ke server
    Request request =
        new Request.Builder().url(BuildConfig.BASE_URL + Api.JENIS_KOMENTAR_ALL).get().build();

    //baris ini -> untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      //baris ini -> method onFailure yang di eksekusi jika terjadi kesalahan saat proses ini dilakukan
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      //baris ini -> method onResponse yang di eksekusi jika proses ini berhasil dilakukan
      @Override public void onResponse(Call call, final Response response) throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  List<Komentar.Jenis> results = new ArrayList<>();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonObject.getInt("rows") > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                      JSONObject dataObject = jsonArray.getJSONObject(i);
                      Komentar.Jenis result = new Komentar.Jenis();
                      result.setId(dataObject.getInt("id"));
                      result.setNama(dataObject.getString("nama"));
                      results.add(result);
                    }
                  }

                  view.onSuccess(results,
                      jsonObject.getInt("rows") > 0 ? Message.LIST_RESULT_SUCCESS
                          : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.LIST_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //interface AllJenisView untuk memberikan tanda ketika proses pengambilan semua data terjadi ke dalam activity atau fragment
  public interface AllJenisView extends BaseView<List<Komentar.Jenis>> {
  }
}
