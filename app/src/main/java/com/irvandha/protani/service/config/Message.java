package com.irvandha.protani.service.config;

/**
 * kelas ini berisi konstan string yang digunakan untuk default pesan saat mengambil data dari server
 *
 * konstan adalah sebuah pernyataan yang tidak boleh diubah lagi
 */
public interface Message {
  //baris ini -> statement untuk menyimpan pesan jika pengambilan semua data berhasil
  String LIST_RESULT_SUCCESS = "Data sukses diambil";
  //baris ini -> statement untuk menyimpan pesan jika pengambilan semua data gagal
  String LIST_RESULT_FAILED = "Data gagal diambil";
  //baris ini -> statement untuk menyimpan pesan jika pengambilan semua data tidak ditemukan
  String LIST_RESULT_NOT_FOUND = "Data tidak ditemukan";
  //baris ini -> statement untuk menyimpan pesan jika proses menyimpan data berhasil
  String SAVE_RESULT_SUCCESS = "Data berhasil disimpan";
  //baris ini -> statement untuk menyimpan pesan jika proses menyimpan data gagal
  String SAVE_RESULT_FAILED = "Data gagal disimpan";
  //baris ini -> statement untuk menyimpan pesan jika terjadi kesalahan saat proses menyimpan data
  String SAVE_RESULT_NOT_FOUND = "Terjadi kesalahan saat menyimpan";
  //baris ini -> statement untuk menyimpan pesan jika proses menghapus data berhasil
  String DELETE_RESULT_SUCCESS = "Data berhasil dihapus";
  //baris ini -> statement untuk menyimpan pesan jika proses menghapus data gagal
  String DELETE_RESULT_FAILED = "Data gagal dihapus";
  //baris ini -> statement untuk menyimpan pesan jika terjadi kesalahan saat proses menghapus data
  String DELETE_RESULT_NOT_FOUND = "Terjadi kesalahan saat menghapus";
  //baris ini -> statement untuk menyimpan pesan jika proses memperbarui data berhasil
  String UPDATE_RESULT_SUCCESS = "Data berhasil diperbarui";
  //baris ini -> statement untuk menyimpan pesan jika proses memperbarui data gagal
  String UPDATE_RESULT_FAILED = "Data gagal diperbarui";
  //baris ini -> statement untuk menyimpan pesan jika terjadi kesalahan saat proses memperbarui data
  String UPDATE_RESULT_NOT_FOUND = "Terjadi kesalahan saat memperbarui";
}
