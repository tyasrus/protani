package com.irvandha.protani.service;

import android.app.Activity;
import android.os.NetworkOnMainThreadException;
import com.irvandha.protani.BuildConfig;
import com.irvandha.protani.model.Komentar;
import com.irvandha.protani.model.Tanaman;
import com.irvandha.protani.service.config.Api;
import com.irvandha.protani.service.config.BaseView;
import com.irvandha.protani.service.config.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * kelas ini digunakan untuk manipulasi data dari atau ke server khusus untuk tabel komentar
 *
 * referensi : https://github.com/square/okhttp/wiki/Recipes
 * mohon dibaca link nya om
 */
public class KomentarService {

  private OkHttpClient client;
  private RequestBody body;
  private Request request;
  private Activity activity;

  /**
   * method getInstance untuk membuat bentuk singleton dari kelas ini
   *
   * @param activity parameter untuk melempar context activity ke dalam kelas ini
   * @return
   */
  public static KomentarService getInstance(Activity activity) {
    //baris ini -> inisiasi variabel KomentarService sebagai instance dari kelas ini
    KomentarService instance = new KomentarService();
    //baris ini -> statement untuk mengecek apakah variabel client belum di inisiasi
    if (instance.client == null) {
      //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //baris ini -> statement untuk set level dari log ini
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      //baris ini -> statement untuk inisiasi variabel client dengan interceptor
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    //baris ini -> statement untuk set variabel activity
    instance.activity = activity;

    //baris ini -> statement untuk nilai kembalian dari method inis
    return instance;
  }

  //method saveOrUpdate untuk menyimpan atau memperbarui data komentar
  //method ini akan berfungsi menyimpan data komentar jika id dari data komentar yang dikirim belum ada atau bernilai 0
  //method ini akan berfungsi memperbarui data komentar jika id dari data komentar yang dikirim sudah ada atau bernilai lebih dari 0
  public void saveOrUpdate(final SaveView view, final Komentar param) {
    //baris ini -> inisiasi variabel body untuk digunakan sebagai wadah dat yg akan dikirim ke server
    body = new FormBody.Builder().add("id_jenis_komentar", String.valueOf(param.getJenis().getId()))
        .add("id_tanaman", String.valueOf(param.getTanaman().getId()))
        .add("nama", param.getNama())
        .add("gejala", param.getGejala())
        .add("perawatan", param.getPerawatan())
        .add("nama_pengirim", param.getNamaPengirim())
        .add("email_pengirim", param.getEmailPengirim())
        .add("status", String.valueOf(param.isStatus()))
        .build();

    //baris ini -> inisiasi variabel request untuk digunakan sebagai penghubung data ke server
    request = new Request.Builder().url(BuildConfig.BASE_URL + Api.KOMENTAR_SAVE)
        //http request post
        .post(body).build();

    //baris ini -> statement untuk mengecek apakah parameter ini mempunyai id atau tidak
    if (param.getId() != 0) {
      //baris ini -> inisiasi variabel body untuk digunakan sebagai wadah dat yg akan dikirim ke server
      body = new FormBody.Builder().add("id", String.valueOf(param.getId()))
          .add("id_jenis_komentar", String.valueOf(param.getJenis().getId()))
          .add("id_tanaman", String.valueOf(param.getTanaman().getId()))
          .add("nama", param.getNama())
          .add("gejala", param.getGejala())
          .add("perawatan", param.getPerawatan())
          .add("nama_pengirim", param.getNamaPengirim())
          .add("email_pengirim", param.getEmailPengirim())
          .add("status", String.valueOf(param.isStatus()))
          .build();

      //baris ini -> inisiasi variabel request untuk digunakan sebagai penghubung data ke server
      request = new Request.Builder().url(BuildConfig.BASE_URL + Api.KOMENTAR_UPDATE)
          //http request post
          .post(body).build();
    }

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      //baris ini -> method onFailure yang di eksekusi jika terjadi kesalahan saat proses ini dilakukan
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      //baris ini -> method onResponse yang di eksekusi jika proses ini berhasil dilakukan
      @Override public void onResponse(Call call, final Response response) throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                //Log.d(getClass().getName(), "response : " + response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(param.getId() != 0 ? Message.UPDATE_RESULT_SUCCESS
                      : Message.SAVE_RESULT_SUCCESS);
                } else {
                  view.onFailed(param.getId() != 0 ? Message.UPDATE_RESULT_FAILED
                      : Message.SAVE_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(param.getId() != 0 ? Message.UPDATE_RESULT_NOT_FOUND
                    : Message.UPDATE_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //method get untuk mengambil data dari tabel komentar berdasarkan id
  public void get(final GetView view, Komentar param) {
    request =
        new Request.Builder().url(BuildConfig.BASE_URL + Api.KOMENTAR_GET + "?id=" + param.getId())
            .get()
            .build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  Komentar result = null;
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonArray.length() > 0) {
                    if (jsonArray.get(0) != null) {
                      result = new Komentar();
                      JSONObject dataObject = jsonArray.getJSONObject(0);
                      result.setId(dataObject.getInt("id"));
                      result.setNama(dataObject.getString("nama"));
                      result.setGejala(dataObject.getString("gejala"));
                      result.setPerawatan(dataObject.getString("perawatan"));
                      result.setNamaPengirim(dataObject.getString("nama_pengirim"));
                      result.setEmailPengirim(dataObject.getString("email_pengirim"));
                      result.setStatus(dataObject.getBoolean("status"));

                      JSONObject tanamanObject = dataObject.getJSONObject("tanaman");
                      Tanaman tanaman = new Tanaman(tanamanObject.getInt("id"));
                      tanaman.setNama(tanamanObject.getString("nama"));
                      result.setTanaman(tanaman);

                      JSONObject jenisObject = dataObject.getJSONObject("jenis");
                      Komentar.Jenis jenis = new Komentar.Jenis(jenisObject.getInt("id"));
                      jenis.setNama(jenisObject.getString("nama"));
                      result.setJenis(jenis);
                    }
                  }

                  view.onSuccess(result,
                      result != null ? Message.LIST_RESULT_SUCCESS : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.LIST_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //method getAll untuk mengambil semua data dari tabel komentar
  public void getAll(final AllView view, Komentar param) {
    String query = BuildConfig.BASE_URL + Api.KOMENTAR_ALL;

    if (param.getTanaman() != null
        && param.getJenis() != null
        && param.getTanaman().getId() != 0
        && param.getJenis().getId() != 0) {
      query =
          query + "?id_tanaman=" + param.getTanaman().getId() + "&id_jenis_komentar=" + param.getJenis()
              .getId();
    }

    request = new Request.Builder().url(query).get().build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  List<Komentar> results = new ArrayList<>();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonObject.getInt("rows") > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                      JSONObject dataObject = jsonArray.getJSONObject(i);
                      Komentar result = new Komentar();
                      result.setId(dataObject.getInt("id"));
                      result.setNama(dataObject.getString("nama"));
                      result.setGejala(dataObject.getString("gejala"));
                      result.setPerawatan(dataObject.getString("perawatan"));
                      result.setNamaPengirim(dataObject.getString("nama_pengirim"));
                      result.setEmailPengirim(dataObject.getString("email_pengirim"));
                      result.setStatus(dataObject.getBoolean("status"));

                      JSONObject tanamanObject = dataObject.getJSONObject("tanaman");
                      Tanaman tanaman = new Tanaman(tanamanObject.getInt("id"));
                      tanaman.setNama(tanamanObject.getString("nama"));
                      result.setTanaman(tanaman);

                      JSONObject jenisObject = dataObject.getJSONObject("jenis");
                      Komentar.Jenis jenis = new Komentar.Jenis(jenisObject.getInt("id"));
                      jenis.setNama(jenisObject.getString("nama"));
                      result.setJenis(jenis);

                      results.add(result);
                    }
                  }

                  view.onSuccess(results,
                      jsonObject.getInt("rows") > 0 ? Message.LIST_RESULT_SUCCESS
                          : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.LIST_RESULT_FAILED);
              }
            }
          }
        });
      }
    });
  }

  //method delete untuk menghapus data dari tabel komentar berdasarkan id
  public void delete(final DeleteView view, Komentar tempat) {
    body = new FormBody.Builder().add("id", String.valueOf(tempat.getId())).build();

    request =
        new Request.Builder().url(BuildConfig.BASE_URL + Api.KOMENTAR_DELETE).post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(jsonObject.getString("status"));
                } else {
                  view.onFailed(Message.DELETE_RESULT_FAILED);
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.DELETE_RESULT_NOT_FOUND);
              }
            }
          }
        });
      }
    });
  }

  //interface DeleteView untuk memberikan tanda ketika proses menghapus data terjadi ke dalam activity atau fragment
  public interface DeleteView {
    void onSuccess(String message);

    void onFailed(String message);
  }

  //interface AllView untuk memberikan tanda ketika proses pengambilan semua data terjadi ke dalam activity atau fragment
  public interface AllView extends BaseView<List<Komentar>> {
  }

  //interface SaveView untuk memberikan tanda ketika proses penyimpanan data terjadi ke dalam activity atau fragment
  public interface SaveView {
    void onSuccess(String message);

    void onFailed(String message);
  }

  //interface GetView untuk memberikan tanda ketika proses pengambilan data terjadi ke dalam activity atau fragment
  public interface GetView {
    void onSuccess(Komentar komentar, String message);

    void onFailed(String message);
  }
}
