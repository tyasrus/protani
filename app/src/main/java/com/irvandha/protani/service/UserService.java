package com.irvandha.protani.service;

import android.app.Activity;
import android.os.NetworkOnMainThreadException;
import com.irvandha.protani.BuildConfig;
import com.irvandha.protani.model.User;
import com.irvandha.protani.service.config.Api;
import com.irvandha.protani.service.config.BaseView;
import com.irvandha.protani.service.config.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * kelas ini digunakan untuk manipulasi data dari atau ke server khusus untuk tabel user
 *
 * referensi : https://github.com/square/okhttp/wiki/Recipes
 * mohon dibaca link nya om
 */
public class UserService {

  private OkHttpClient client;
  private RequestBody body;
  private Request request;
  private Activity activity;

  /**
   * method getInstance untuk membuat bentuk singleton dari kelas ini
   *
   * @param activity parameter untuk melempar context activity ke dalam kelas ini
   * @return
   */
  public static UserService getInstance(Activity activity) {
    //baris ini -> inisiasi variabel UserService sebagai instance dari kelas ini
    UserService instance = new UserService();
    //baris ini -> statement untuk mengecek apakah variabel client belum di inisiasi
    if (instance.client == null) {
      //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //baris ini -> statement untuk set level dari log ini
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      //baris ini -> statement untuk inisiasi variabel client dengan interceptor
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    //baris ini -> statement untuk set variabel activity
    instance.activity = activity;

    //baris ini -> statement untuk nilai kembalian dari method inis
    return instance;
  }

  //untuk melakukan pengecekan user atau login ke server
  public void login(final LoginView view, final User user) {
    //FormBody.Builder ini bertujuan untuk mengirim data dengan beberapa parameter ke server
    body = new FormBody.Builder().add("name", user.getName())
        .add("password", user.getPassword())
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(BuildConfig.BASE_URL + Api.USER_GET)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      //baris ini -> method onFailure yang di eksekusi jika terjadi kesalahan saat proses ini dilakukan
      @Override public void onFailure(Call call, final IOException e) {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      //baris ini -> method onResponse yang di eksekusi jika proses ini berhasil dilakukan
      @Override public void onResponse(Call call, final Response response) throws IOException {
        activity.runOnUiThread(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  JSONObject dataObject = jsonArray.getJSONObject(0);
                  User result = new User();
                  result.setId(dataObject.getInt("id"));
                  view.onSuccess(result, jsonObject.getString("status"));
                } else {
                  view.onFailed("Akun tidak valid");
                }
              } catch (NetworkOnMainThreadException | JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Akun tidak valid");
              }
            }
          }
        });
      }
    });
  }

  //interface LoginView untuk memberikan tanda ketika proses login terjadi ke dalam activity atau fragment
  public interface LoginView extends BaseView<User> {
  }
}
