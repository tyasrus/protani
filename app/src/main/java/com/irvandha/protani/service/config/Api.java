package com.irvandha.protani.service.config;
/**
 * kelas ini berisi konstan string yang digunakan untuk tambahan url sesuai kebutuhan
 *
 * konstan adalah sebuah pernyataan yang tidak boleh diubah lagi
 */
public interface Api {
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan user untuk login
    String USER_GET = "user/get.php";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk membuat komentar baru
    String KOMENTAR_SAVE = "komentar/save.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil data komentar sesuai dengan id
    String KOMENTAR_GET = "komentar/get.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil semua data komentar sesuai dengan id
    String KOMENTAR_ALL = "komentar/list.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk memperbarui data komentar sesuai dengan id
    String KOMENTAR_UPDATE = "komentar/update.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menghapus data komentar sesuai dengan id
    String KOMENTAR_DELETE = "komentar/delete.php";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk membuat tanaman baru
    String TANAMAN_SAVE = "tanaman/save.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil data tanaman sesuai dengan id
    String TANAMAN_GET = "tanaman/get.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil semua data tanaman sesuai dengan id
    String TANAMAN_ALL = "tanaman/list.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk memperbarui data tanaman sesuai dengan id
    String TANAMAN_UPDATE = "tanaman/update.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menghapus data tanaman sesuai dengan id
    String TANAMAN_DELETE = "tanaman/delete.php";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk membuat jenis komentar baru
    String JENIS_KOMENTAR_SAVE = "komentar/jenis/save.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil data jenis komentar sesuai dengan id
    String JENIS_KOMENTAR_GET = "komentar/jenis/get.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil semua data jenis komentar sesuai dengan id
    String JENIS_KOMENTAR_ALL = "komentar/jenis/list.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk memperbarui data jenis komentar sesuai dengan id
    String JENIS_KOMENTAR_UPDATE = "komentar/jenis/update.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menghapus data jenis komentar sesuai dengan id
    String JENIS_KOMENTAR_DELETE = "komentar/jenis/delete.php";
}
