package com.irvandha.protani.service.config;

/**
 * kelas ini adalah bentuk dasar untuk view pada activity atau fragment
 *
 * @param <T> general parameter untuk model atau object dalam java
 */
public interface BaseView<T> {
    //baris ini ->  method onSuccess berfungsi jika suatu process telah berhasil dilakukan
    void onSuccess(T t, String message);
    //baris ini ->  method onFailed berfungsi jika suatu process tidak berhasil dilakukan
    void onFailed(String message);
}
